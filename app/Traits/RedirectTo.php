<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/20/2017
 * Time: 12:58 AM
 */

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait RedirectTo
{
    public function RedirectUserTo($user_id){
        $user_details = DB::table('emuhaya_users')
            ->where('id',$user_id)
            ->first();


        if($user_details->role_id == 1){
            return 'admin';
        }
        elseif ($user_details->role_id == 2){
            return 'user';
        }
        else{
            return view('index');
        }

    }
}