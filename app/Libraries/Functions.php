<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/15/2017
 * Time: 12:26 PM
 */

namespace App\Libraries;


class Functions
{
    public function generateRandomNumber($length)
    {
        $characters = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';

        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }

        return $string;
    }

}