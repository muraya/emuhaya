<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/25/2017
 * Time: 6:36 PM
 */


    // Reads the variables sent via POST from our gateway
    $session_Id   = $_POST["sessionId"];
    $service_code = $_POST["serviceCode"];
    $phone_number = $_POST["phoneNumber"];
    $ussd_text   = $_POST["text"];

    $name = '';
    $gender = '';
    $email = '';
    $marital_status = '';
    $about = '';
    $membership = '';
    $accept_deny = '';


    $level = 0;

    if($ussd_text != "")
    {
        $text=  str_replace("#", "*", $ussd_text);
        $ussd_string_explode = explode("*", $ussd_text);
        $level = count($ussd_string_explode);
    }

    if ($level==0){
        displaymenu();
    }

    function getLevel()
    {

    }

    function setLevel($level)
    {
        return $level;
    }

    function displayMenu()
    {
        $ussd_text  = "CON Welcome Home . Please reply with \n";
        $ussd_text .= "1. Register \n";
        $ussd_text .= "2. Unsubscribe \n";
        $ussd_text .= "3. About Us";
        ussdProceed($ussd_text);
    }

    function ussdProceed($ussd_text)
    {
        echo $ussd_text;
    }

    function ussdEnd($ussd_text)
    {
        echo 'END '.$ussd_text;
    }

    if ($level>0){
        switch ($ussd_string_explode[0])
        {
            case 1:
                register($ussd_string_explode,$phone_number);
                break;
            case 2:
                about();
                break;
            default: ussdEnd("you entered the wrong value");
        }
    }

    function register($details,$phone_number){

        if (count($details)==1){
            $ussd_text="CON Enter your Full Name";
            ussdProceed($ussd_text);
        }
        else if(count($details) == 2){
            $ussd_text = "CON Enter your Email";
            ussdProceed($ussd_text);
        }
        else if(count($details) == 3){

            $ussd_text = "CON  Select Gender \n";
            $ussd_text .= "1. To select male \n";
            $ussd_text .= "2. To select female\n";
            ussdProceed($ussd_text);
        }else if(count($details) == 4){

            $ussd_text = "CON Select Marital Status \n";
            $ussd_text .= "1. To select Married \n";
            $ussd_text .= "2. To select Single \n";
            $ussd_text .= "3. To select Divorced \n";
            ussdProceed($ussd_text);
        }else if(count($details) == 5){

            $ussd_text = "CON Your Residence \n";
            ussdProceed($ussd_text);
        }else if(count($details) == 6){

            $ussd_text = "CON How did you hear about us \n";
            $ussd_text .= "1. To select Friend \n";
            $ussd_text .= "2. To select Social Media \n";
            $ussd_text .= "3. To select Website\n";
            ussdProceed($ussd_text);
        }else if(count($details) == 7) {

            $ussd_text = "CON Would you like to be a member \n";
            $ussd_text .= "1. Yes \n";
            $ussd_text .= "2. No \n";
            $ussd_text .= "3. Maybe \n";
            ussdProceed($ussd_text);
        }
        else if(count($details) == 8)
        {
            $name=$details[1];
            $email=$details[2];
            $gender=$details[3];
            $marital_status=$details[4];
            $residence=$details[5];
            $about=$details[6];
            $membership=$details[7];

            if($gender=="1"){
                $gender="Male";
            }else if($gender=="2"){
                $gender="Female";
            }

            if($marital_status == "1"){
                $marital_status="Married";
            }else if($marital_status=="2"){
                $marital_status="Single";
            }else if($marital_status=="3"){
                $marital_status="Divorced";
            }

            if($about == "1"){
                $about="Friend";
            }else if($about=="2"){
                $about="Social Media";
            }else if($about=="3"){
                $about="Website";
            }

            if($membership == "1"){
                $membership="Yes";
            }else if($membership=="2"){
                $membership="No";
            }else if($membership=="3"){
                $membership="Maybe";
            }

            $ussd_text = "CON Please Confirm Details \n";
            $ussd_text .= "1.$name \n";
            $ussd_text .= "2.$email \n";
            $ussd_text .= "3.$gender \n";
            $ussd_text .= "4.$marital_status \n";
            $ussd_text .= "5.$about \n";
            $ussd_text .= "6.$residence \n";
            $ussd_text .= "6.$membership \n";
            $ussd_text .= "7.Cancel \n";
            $ussd_text .= "8.Register \n";
            ussdProceed($ussd_text);
        }

        else if(count($details) == 9)
        {
            $proceed = $details[8];

            if($proceed !='7' && $proceed!='8')
            {
                ussdEnd('You selected the wrong option');
            }
            else if($proceed == '7')
            {
                ussdEnd('Thank you for reaching us');
            }
            else if($proceed == '8')
            {

                $name=$details[1];
                $email=$details[2];
                $gender=$details[3];
                $marital_status=$details[4];
                $residence=$details[5];
                $about=$details[6];
                $membership=$details[7];

                if($gender=="1"){
                    $gender="Male";
                }else if($gender=="2"){
                    $gender="Female";
                }

                if($marital_status == "1"){
                    $marital_status="Married";
                }else if($marital_status=="2"){
                    $marital_status="Single";
                }else if($marital_status==""){
                    $marital_status="Divorced";
                }

                if($about == "1"){
                    $about="Friend";
                }else if($about=="2"){
                    $about="Social Media";
                }else if($about==""){
                    $about="Podcast";
                }

                if($membership == "1"){
                    $membership="Yes";
                }else if($membership=="2"){
                    $membership="No";
                }else if($membership==""){
                    $membership="Maybe";
                }

                $hostname='localhost';
                $database='devinven_schoolerp';
                $username='devinven_root';
                $password='awesome';

                try {
                    $conn = new PDO("mysql:host=$hostname;dbname=".$database,$username,$password);

                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    error_log('Connected to Database');

                    try {
                        $sql = "INSERT INTO church_members (member_name, gender, phone, email, about, membership, m_status) VALUES ($name,$gender, $phone_number, $email, $about, $membership, $marital_status)";
                        $conn->exec($sql);
//                        if($sth){
//                            registerSuccess();
//                        }
//                        else{
//                            registerFailure();
//                        }
                    } catch(PDOException $e) {
                        error_log( $e->getMessage());
                        registerFailure();
                    }

                    $dbh = null;
                }
                catch(PDOException $e)
                {
                    error_log( $e->getMessage());
                    registerFailure();
                }
            }
        }
    }

    function registerSuccess()
    {
        $ussd_text = "END Thank you for registering with us\n";
        ussdProceed($ussd_text);
    }

    function registerFailure()
    {
        $ussd_text = "END We have encountered a problem, Please Try again\n";
        ussdProceed($ussd_text);
    }

    function about()
    {
        $ussd_text = "END We are the best place in the world \n";
        ussdProceed($ussd_text);
    }

    function connect()
    {
        $link = mysqli_connect("localhost", "devinven_root", "awesome", "devinven_schoolerp");

// Check connection
        if($link === false){
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }
    }

    function insert()
    {
        // Attempt insert query execution



// Close connection



    }



//header('Content-type: text/plain');
//echo $response;
// DONE!!!
?>