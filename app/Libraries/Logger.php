<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/19/2017
 * Time: 10:44 AM
 */

namespace App\Libraries;


class Logger
{
    public function WriteLog($logStream){
        $_LOGFILE = 'LogData.log';

        $file = fopen($_LOGFILE, 'a');
        fwrite($file, '['.date('D M j G:i:s T Y').'] '.$logStream.'\n');
        fclose($file);
    }
}