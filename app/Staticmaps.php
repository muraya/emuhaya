<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staticmaps extends Model
{
    //
    protected $table = 'emuhaya_static_maps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'description', 'image', 'status', 'created_at'];
}
