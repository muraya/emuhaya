<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends BaseControler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:emuhaya_users,email|max:255',
            'phone' => 'required|unique:emuhaya_users,phone|numeric',
        ]);
        //
        if($validator -> passes())
        {
            $User = new User();

            $User->name = $request->input('name');
            $User->email = $request->input('email');
            $User->phone = $request->input('phone');
            $User->role_id = $request->input('role');
            $User->password = Hash::make('secret');
            $User -> avatar = " ";
            $User->status = 1; /// status for new user
            $User->created_at = Carbon::now();

            if ($User->save())
            {
                return response()->json(['status' => '00', 'message' => 'Created User Successfully']);

            }
            else{
                return response()->json(['status' => '01', 'message' => 'Error when creating user']);
            }
        }
        else{

            return response()->json(['status' => '01', 'message' => $this -> returnMessageString($validator->errors()->getMessages())]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $user = User::find($id);

        $response = array(
            "status" => "00",
            "id" => $user['id'],
            "name" => $user['name'],
            "email" => $user['email'],
            "phone" => $user['phone'],
            "role" => $user['role_id'],
        );

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::findOrFail($id);
        $input = $request->all();
        $user->fill($input)->save();

        return response()->json(['status' => '00', 'message' => 'User Details changes have been updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json(['status' => '00', 'message' => 'User has been Deleted Successfully']);
    }

    public function usersData()
    {
        $users = User::all();


    }


    public function activate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_users')
            ->where('id', $id)
            ->update(['status' => 1]);

        return response()->json(['status' => '00', 'message' => 'User has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_users')
            ->where('id', $id)
            ->update(['status' => 2]);

        return response()->json(['status' => '00', 'message' => 'User has been Deactivated Successfully']);

    }

}
