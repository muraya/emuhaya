<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function generateCategories()
    {
        $categories = DB::table('emuhaya_categories')->get();

        foreach ($categories as $category) {
            $markers = DB::table('markers')->where('point_type','=',$category -> name)->get();

            foreach ($markers as $marker) {
                DB::table('emuhaya_markers')
                    ->where('id', $marker -> point_id)
                    ->update(['category_id' => $category -> id]);
            }
            
        }

        return 'done';
    }

}
