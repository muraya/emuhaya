<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class RoleController extends BaseControler
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $User = new Role();

        $User->name = $request->input('name');
        $User->slug = $request->input('slug');
        $User->description = $request->input('description');
        $User->created_at = Carbon::now();

        if ($User->save())
        {
            return response()->json(['status' => '00', 'message' => 'Created Role Successfully']);

        }
        else{
            return response()->json(['status' => '01', 'message' => 'Error when creating Role']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $role = Role::findOrFail($id);

        return view('admin.roles.show')->with('role',$role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::find($id);

        $response = array(
            "status" => "00",
            "id" => $role['id'],
            "name" => $role['name'],
            "slug" => $role['slug'],
            "description" => $role['description'],
        );

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $role = Role::findOrFail($id);
        $input = $request->all();
        $role->fill($input)->save();

        return response()->json(['status' => '00', 'message' => 'Role Details changes have been updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Role::findOrFail($id);
        $role->delete();

        return response()->json(['status' => '00', 'message' => 'Role has been Deleted Successfully']);
    }

    public function rolesData()
    {

        $actions ='

                <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal" data-id="{{ $id }}"  
                data-name="{{$name}}" data-target="#editModal" > <i class="fa fa-edit"></i></a>
                 ';

        $roles = Role::query();

        return Datatables::of($roles)
            ->addColumn('actions', $actions)
            ->rawColumns(['actions', 'actions'])
            ->make(true);

    }
}
