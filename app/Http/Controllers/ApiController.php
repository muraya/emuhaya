<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Libraries\AfricasTalkingGateway;
use App\Libraries\Functions;
use App\Points;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ApiController extends Controller
{
    //
    //<editor-fold desc="Mobile API calls handlers">
    public function login(Request $request)
    {
        $phone = $request->input('phone');
        $password = $request->input('password');

        if(!empty($phone) OR !empty($password))
        {

            $user = DB::table('emuhaya_users')
                ->where('phone','=',$phone)
                ->first();
            if(!empty($user))
            {
                if(Hash::check($password, $user->password))
                {

                    if($user -> status == 1){
                        // user has active account
                        $response = [
                            'login' => 'Success',
                            'status' => '200',
                            'message' => 'Successfully Logged In',
                            'user_id' => $user -> id,
                            'name' => $user -> name,
                            'email' => $user -> email,
                            'phone' => $user -> phone,
                        ];
                    }
                    else{
                        //
                        $response = [
                            "login" => "Failed",
                            'status' => '313',
                            'message' => 'Your account is inactive'
                        ];
                    }
                }
                else
                {
                    //username and password details mismatch
                    $response = array(
                        "login" => "Failed",
                        "status" => "312", // Incorrect username/password combination
                        "message" => "Incorrect username and password combination" // Incorrect username/password combination
                    );
                }
            }
            else
            {
                // the user does not exist
                $response = array(
                    "login"=>"Failed",
                    "status" => "311",// User not registered
                    "message" => "Seems you're not registered"// User not registered
                );
            }
        }
        else
        {
            // either the username or the password is not provided
            $response = array(
                "login" => "Failed",
                "status" => "310", // code for wrong request
                "message" => "Empty username or password", // code for wrong request
            );
        }

        return json_encode($response);
    }

    public function register(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');


        if(!empty($name) AND !empty($email) AND !empty($phone))
        {

            $user = DB::table('emuhaya_users')
                ->where('phone','=',$phone)
                ->first();

            if(empty($user))
            {

                $User = new User();

                $User -> role_id = 2;
                $User -> name = $name;
                $User -> phone = $phone;
                $User -> email = $email;
                $User -> avatar = " ";
                $User -> password = bcrypt('secret');
                $User -> status = 1;
                $User -> created_at = Carbon::now();


//                $mainProfilePicture = public_path('uploads/profile');
//                $webProfilePicture = public_path('uploads/profile/web');
//                $mobileProfilePicture = public_path('uploads/profile/mobile');
//                $image = $request -> file('image');
//
//                if(!empty($image))
//                {
//
//                    $extension = $image->getClientOriginalExtension();
//                    $fileName = Carbon::now()->getTimestamp().'.'.$extension;
//                    $request->image->move($mainProfilePicture, $fileName);
//
//                    $img = Image::make($image->getRealPath());
//
//                    $img->encode('jpg', 90)->fit(170)->save($webProfilePicture.'/'.$fileName);
//
//                    $img->encode('jpg', 90)->fit(128)->save($mobileProfilePicture.'/'.$fileName);
//
//                    $User -> photo = $fileName;
//
//                }
//                else{
//                    $User -> photo = '';
//                }

                if($User -> save())
                {
                    // user successfully registered
                    $response = [
                        'register' => 'Success',
                        'registerstatus' => '200',
                        'registermessage' => 'You account has been created',
                    ];
                }
                else{
                    // the user has errors being registered
                    $response = array(
                        "register"=>"Failed",
                        "registerstatus" => "317",// User not registered
                        "registermessage" => "Some Error Creating your account. Try again please"
                    );
                }

            }
            else
            {
                // the user already registered
                $response = array(
                    "register"=>"Failed",
                    "registerstatus" => "316",
                    "registermessage" => "Your account already exists"
                );
            }
        }
        else
        {
            // registration parameters missing
            $response = array(
                "register" => "Failed",
                "registerstatus" => "315", // code for wrong request
                "registermessage" => "Please fill up all the fields", // code for wrong request
            );
        }

        return json_encode($response);
    }

    public function reset(Request $request)
    {
        $phone = $request->input('phone');


        if($phone)
        {

            $user = DB::table('emuhaya_users')
                ->where('phone','=',$phone)
                ->first();

            if(!empty($user))
            {


                $Functions = new Functions();
                $password = $Functions -> generateRandomNumber(6);


                $gateway    = new AfricasTalkingGateway(env('AFRICAS_TALKING_USERNAME'), env('AFRICAS_TALKING_API_KEY'));

                $message    = "Your new password for the emuhaya mobile application is ".$password;

                if($gateway->sendMessage($user->phone, $message))
                {

                    DB::table('emuhaya_users')
                        ->where('id', $user->id)
                        ->update([
                            'password' => Hash::make($password),
                            'updated_at' => Carbon::now(),
                        ]);

                    // user successfully registered
                    $response = [
                        'reset' => 'Success',
                        'resetstatus' => '200',
                        'resetmessage' => 'You password has been reset successfully',
                    ];
                }
                else{
                    // the user has errors being registered
                    $response = array(
                        "reset"=>"Failed",
                        "registerstatus" => "317",// User not registered
                        "registermessage" => "Some Error Creating your account. Try again please"
                    );
                }

            }
            else
            {
                // the user does not exist
                $response = array(
                    "reset"=>"Failed",
                    "resetstatus" => "316",
                    "resetmessage" => "Your account does not exist"
                );
            }
        }
        else
        {
            // registration parameters missing
            $response = array(
                "register" => "Failed",
                "registerstatus" => "315", // code for wrong request
                "registermessage" => "Please fill up all the fields", // code for wrong request
            );
        }

        return json_encode($response);
    }

    public function update(Request $request)
    {
        $user_id = $request->input('id');
        $phone = $request->input('phone');
        $email = $request->input('email');

        $user = User::findOrFail($user_id);
        $input = $request->all();



            if($user->fill($input)->save())
            {

                    // user successfully registered
                    $response = [
                        'update' => 'Success',
                        'updatestatus' => '200',
                        'updatemessage' => 'You Account has been updated successfully',
                    ];


            }
            else
            {
                // the user does not exist
                $response = array(
                    "update"=>"Failed",
                    "updatestatus" => "316",
                    "updatemessage" => "We have had issues updating your account.Please try Again"
                );
            }

        return json_encode($response);
    }

    public function getPoints()
    {
        $points = Points::where('status','=',1)->get();

        $points_array = array();

        foreach ($points as $point)
        {
            $value['point_id'] = intval($point['id']);
            $value['name'] = $point['name'];
            $value['category'] = Categories::find($point['category_id'])['name'];
            $value['description'] = $point['description'];
            $value['author'] = User::find($point['user_id'])['name'];
            $value['author_id'] = intval($point['user_id']);
            $value['image'] = $point['image'];
            $value['lat'] = $point['lat'];
            $value['lng'] = $point['lng'];
            $value['created'] = Carbon::createFromTimestamp(strtotime($point['created_at']))->diffForHumans();

            $points_array[] = $value;
        }

        return json_encode($points_array);
    }

    public function getUserPoints(Request $request)
    {
        $user_id = $request -> input('id');

        $points = Points::where('status','=','1')
            ->where('user_id','=',$user_id)
            ->get();

        $points_array = array();

        foreach ($points as $point)
        {
            $value['point_id'] = intval($point['id']);
            $value['name'] = $point['name'];
            $value['category'] = Categories::find($point['category_id'])['name'];
            $value['description'] = $point['description'];
            $value['author'] = User::find($point['user_id'])['name'];
            $value['author_id'] = intval($point['user_id']);
            $value['image'] = $point['image'];
            $value['lat'] = $point['lat'];
            $value['lng'] = $point['lng'];
            $value['created'] = Carbon::createFromTimestamp(strtotime($point['created_at']))->diffForHumans();

            $points_array[] = $value;
        }

        return json_encode($points_array);
    }

    public function postPoint(Request $request)
    {

        $image = $request->file('pic');
        $name =  $request->input('name');
        $description = $request->input('description');
        $category = $request->input('category');
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        $author_id = $request->input('author_id');

        if(
            !empty($image) AND
//            !empty($image_name) AND
            !empty($description) AND
            !empty($category) AND
            !empty($lat) AND
            !empty($lng) AND
            !empty($author_id)
        )
        {

            $Category = Categories::where('name','=',$category)->first();


                $Point = new Points();
                $Point -> user_id = $author_id;
                $Point -> name = $name;
                $Point -> description = $description;
                $Point -> category_id = $Category -> id;
                $Point -> lat = $lat;
                $Point -> lng = $lng;
                $Point -> status = 2;
                $Point -> views = 0;
                $Point -> likes = 0;
                $Point -> created_at = Carbon::now();


                $main_point_path = 'uploads/points';
                $web_point_path = 'uploads/points/web';
                $mobile_point_path = 'uploads/points/mobile';



                    $input['main_pic_name'] = time().'.'.$image->getClientOriginalExtension();
                    $main_pic_img = Image::make($image->getRealPath());
                    $main_pic_img->resize(500, 400)->save($main_point_path.'/'.$input['main_pic_name']);

                    $web_pic_img = Image::make($image->getRealPath());
                    $web_pic_img->resize(300, 300)->save($web_point_path.'/'.$input['main_pic_name']);

                    $mobile_pic_img = Image::make($image->getRealPath());
                    $mobile_pic_img->resize(200, 200)->save($mobile_point_path.'/'.$input['main_pic_name']);

                    $Point -> image = $input['main_pic_name'];



                if($Point -> save())
                {
                    // user successfully registered
                    $response = [
                        'point' => 'Success',
                        'status' => '200',
                        'message' => 'Point Successfully posted',
                    ];
                }
                else{
                    // the user has errors being registered
                    $response = array(
                        "point"=>"Failed",
                        "status" => "317",// User not registered
                        "message" => "Some Error Posting your point. Try again please"
                    );
                }

            }
            else
            {
                // registration parameters missing
                $response = array(
                    "point" => "Failed",
                    "status" => "315", // code for wrong request
                    "message" => "Empty Request Parameters", // code for wrong request
                );
            }

            return json_encode($response);
    }

    /**
     * Get Users
     * @return string
     */

    public function getUsers()
    {
        $users = User::where('status','=','1')->get();

        $users_array = array();

        foreach ($users as $user)
        {
            $value['user_id'] = $user['id'];
            $value['name'] = $user['name'];
            $value['phone'] = $user['phone'];
            $value['email'] = $user['email'];
            $value['user_role'] = Role::find($user['role_id'])['name'];
            $value['profile'] = $user['avatar'];
            $value['created'] = Carbon::createFromTimestamp(strtotime($user['created_at']))->diffForHumans() ;
            $value['points'] = Points::where('user_id','=',$user['user_id'])->count();



            $users_array[] = $value;
        }

        return json_encode($users_array);
    }

    public function getCategories()
    {
        $categories = Categories::where('status','=','1')->get();

        $categories_array = array();

        foreach ($categories as $category)
        {
            $value['name'] = $category['name'];
            $value['description'] = $category['description'];
            $value['image'] = $category['image'];
            $value['added'] = Carbon::createFromTimestamp(strtotime($category['created_at']))->diffForHumans();

            $categories_array[] = $value;
        }

        return json_encode($categories_array);
    }

    //</editor-fold>


    //<editor-fold desc="WEB API calls Handlers">

    public function getWebPoints()
    {
        $points = Points::where('status','=',1)->get();

        $points_array = array();

        foreach ($points as $point)
        {
            $value['id'] = $point['id'];
            $value['name'] = $point['name'];
            $value['title'] = $point['name'];
            $value['category'] = is_null(Categories::find($point['category_id'])['name']) ? '' : Categories::find($point['category_id'])['name'];
            $value['description'] = substr($point['description'], 0, 100);
            $value['tag'] = strtok(Categories::find($point['category_id'])['name'], " ");
            $value['likes'] = $point['likes'];
            $value['thumbnail'] = File::exists(asset('uploads/points/web/'.$point['image'])) ? 'uploads/points/blank.png' : asset('uploads/points/web/'.$point['image']);
            $value['verified'] = $point['status'] == '1' ? true : false; ;
            $value['latitude'] = $point['lat'];
            $value['longitude'] = $point['lng'];

            $points_array[] = $value;
        }

        return json_encode($points_array);
    }

    //</editor-fold>

    public function webPointDetails(Request $request)
    {
        $point_id = $request->input('id');

        $Point = DB::table('emuhaya_points')->where('id','=',$point_id)->first();

        $response = array(
            "status" => "00",
            "id" => $point_id,
            "name" => $Point -> name,
            "category" => is_null(Categories::find($Point -> category_id)['name']) ? '' : Categories::find($Point -> category_id)['name'],
            "description" => $Point -> description ,
            "tag" => strtok(Categories::find($Point -> category_id)['name'], " "),
            "likes" => $Point -> likes,
            "thumbnail" => 'uploads/points/web/'.$Point -> image,
            "verified" => $Point -> status == '1' ? true : false ,
            "latitude" => $Point -> lat,
            "longitude" => $Point -> lng,
    );

        return response()->json($response);

    }


}
