<?php

namespace App\Http\Controllers;

use App\Categories;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class CategoryController extends BaseControler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);
        //
        if($validator -> passes()) {

            $Category = new Categories();

            $Category->user_id = Auth::user()->id;
            $Category->name = $request->input('name');
            $Category->description = $request->input('description');
            $Category->likes = 0;
            $Category->posts = 0;
            $Category->image = '';
            $Category->status = 2; // inactive status on creating a new category
            $Category->created_at = Carbon::now();

            $main_pic = $request->file('image');
            $input['main_pic_name'] = time().'.'.$main_pic->getClientOriginalExtension();
            $main_pic_path = 'uploads/categories';
            $main_pic_img = Image::make($main_pic->getRealPath())
                ->resize(450, 300)
                ->save($main_pic_path.'/'.$input['main_pic_name']);

            $Category->image = $input['main_pic_name'];


            //save order details
            if ($Category->save())
            {
                return Redirect::back()->withSuccess('Category created successfully!');
            }
            else{
                return Redirect::back()->withErrors('Error when creating Category!');
            }
        }
        else{
            return Redirect::back()->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Category = DB::table('emuhaya_categories')
            ->where('id','=',$id)
            ->first();

        return view('admin.categories.edit',compact('Category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Category = Categories::findOrFail($id);
        $input = $request->all();
        $Category->fill($input)->save();

        return Redirect::back()->withSuccess('Category Details changes have been updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $CategoryPosts = DB::table('emuhaya_points')
            ->where('category_id','=',$id)
            ->get();

        if($CategoryPosts)
        {
            return response()->json(['status' => '01', 'message' => 'Category has attached points. Cannot Be deleted']);
        }
        else{
            $Category = Categories::findOrFail($id);
            $Category->delete();

            return response()->json(['status' => '00', 'message' => 'Category Details has been Deleted Successfully']);

        }

    }

    public function activate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_categories')
            ->where('id', $id)
            ->update(['status' => 1]);

        return response()->json(['status' => '00', 'message' => 'Category has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_categories')
            ->where('id', $id)
            ->update(['status' => 2]);

        return response()->json(['status' => '00', 'message' => 'Category has been Deactivated Successfully']);

    }
}
