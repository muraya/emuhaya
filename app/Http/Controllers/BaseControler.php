<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Traits\FormatAjaxValidationMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class BaseControler extends Controller
{
    use FormatAjaxValidationMessages;

    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

}
