<?php

namespace App\Http\Controllers;

use App\Configurations;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ConfigurationController extends BaseControler
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.configurations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $configuration = new Configurations();

        $configuration->name = $request->input('name');
        $configuration->main_address = $request->input('main_address');
        $configuration->other_address = $request->input('other_address');
        $configuration->main_phone = $request->input('main_phone');
        $configuration->other_phone = $request->input('other_phone');
        $configuration->admin_email = $request->input('admin_email');
        $configuration->created_at = Carbon::now();


        if ($configuration->save())
        {
            return back()->with('success','Created Configuration Successfully');
        }
        else
        {
            return back()->with('error','Error Creating Configuration');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $configuration = Configurations::findOrFail($id);
        $input = $request->all();

        if ($configuration->fill($input)->save())
        {
            return back()->with('success','Configuration details updates successfully');
        }
        else
        {
            return back()->with('error','Error updating configurations details ');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
