<?php

namespace App\Http\Controllers;

use App\Staticmaps;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class StaticmapsController extends BaseControler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.staticmaps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);
        //
        if($validator -> passes()) {

            $Staticmap = new Staticmaps();

            $Staticmap->user_id = Auth::user()->id;
            $Staticmap->name = $request->input('name');
            $Staticmap->description = $request->input('description');
            $Staticmap->status = 2; // id for a new point
            $Staticmap->created_at = Carbon::now();

            $main_pic = $request->file('image');
            $input['main_pic_name'] = time().'.'.$main_pic->getClientOriginalExtension();
            $main_pic_path = 'uploads/staticmaps';
            $main_pic_img = Image::make($main_pic->getRealPath())
                ->resize(450, 300)
                ->save($main_pic_path.'/'.$input['main_pic_name']);

            $Staticmap->image = $input['main_pic_name'];



            //save static maps details
            if ($Staticmap->save())
            {
                return Redirect::back()->withSuccess('Static Map posted successfully!');
            }
            else{
                return Redirect::back()->withErrors('Error when Posting Static Map!');
            }
        }
        else{
            return Redirect::back()->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Staticmap = DB::table('emuhaya_static_maps')
            ->where('id','=',$id)
            ->first();

        return view('admin.staticmaps.edit',compact('Staticmap'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Staticmap = Staticmaps::findOrFail($id);
        $input = $request->all();
        $Staticmap->fill($input)->save();

        return Redirect::back()->withSuccess('Static Map Details changes have been updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Staticmaps = Staticmaps::findOrFail($id);
        $Staticmaps->delete();

        return response()->json(['status' => '00', 'message' => 'Static Map Details has been Deleted Successfully']);
    }




    public function activate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_static_maps')
            ->where('id', $id)
            ->update(['status' => 1]);

        return response()->json(['status' => '00', 'message' => 'Static Map has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_static_maps')
            ->where('id', $id)
            ->update(['status' => 2]);

        return response()->json(['status' => '00', 'message' => 'Static Map has been Deactivated Successfully']);

    }
}
