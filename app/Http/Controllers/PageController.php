<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Points;
use App\Staticmaps;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    //

    public function index()
    {
        $points = Points::leftjoin('emuhaya_categories','emuhaya_points.category_id','=','emuhaya_categories.id')
            ->where('emuhaya_points.status','=',1)
            ->select('emuhaya_points.*','emuhaya_categories.name as category')
            ->limit(15)
            ->inRandomOrder()
            ->get();

        $categories = Categories::where('status','=',1)->get();

        return view('index',compact('points','categories'));
    }

    public function mapped()
    {
        $points = Points::leftjoin('emuhaya_categories','emuhaya_points.category_id','=','emuhaya_categories.id')
            ->where('emuhaya_points.status','=',1)
            ->select('emuhaya_points.*','emuhaya_categories.name as category')
            ->get();

        $categories = Categories::where('status','=',1)->get();

        return view('mapped',compact('points','categories'));
    }

    public function listed()
    {
        $points = Points::leftjoin('emuhaya_categories','emuhaya_points.category_id','=','emuhaya_categories.id')
            ->where('emuhaya_points.status','=',1)
            ->select('emuhaya_points.*','emuhaya_categories.name as category')
            ->paginate(10);

        $categories = Categories::leftjoin('emuhaya_points','emuhaya_categories.id','=','emuhaya_points.category_id')
            ->where('emuhaya_categories.status','=',1)
            ->selectRaw('emuhaya_categories.name, count(emuhaya_points.id) as points_count')
            ->groupBy('emuhaya_categories.name')
            ->get();

        return view('listed',compact('points','categories'));
    }

    public function listedDetails($id)
    {
        $Point = Points::where('id', $id)
            ->firstOrFail();

        $User = DB::table('emuhaya_users')
            ->where('id','=',$Point -> user_id)
            ->first();

        $listings = DB::table('emuhaya_points')
            ->where('user_id','=',$Point -> user_id)
            ->get();

        $categories = Categories::where('status','=',1)->get();

        return view('details',compact('Point','User','listings','categories'));
    }

    public function category()
    {
        $categories = DB::table('emuhaya_categories')
            ->where('status','=',1)
            ->paginate(10);
        return view('category',compact('categories'));
    }

    public function staticMap($id = null)
    {
        if($id){
            $categories = Categories::where('status','=',1)->get();

            $static_maps = Staticmaps::where('status','=',1) -> get();

            $StaticMap = Staticmaps::where('id','=',$id)
                ->first();

            return view('staticmap',compact('categories','static_maps','StaticMap'));
        }
        else {

            $StaticMap = Staticmaps::where('status','=',1)
                ->inRandomOrder()
                ->first();

            $categories = Categories::where('status','=',1)->get();

            $static_maps = Staticmaps::where('status','=',1) -> get();

            return view('staticmap',compact('categories','static_maps','StaticMap'));
        }

    }

    public function webMaps()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.index',compact('categories'));
    }

    public function bridges()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.bridges',compact('categories'));
    }

    public function building()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.building',compact('categories'));
    }

    public function churches()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.churches',compact('categories'));
    }

    public function contours()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.contours',compact('categories'));
    }

    public function health()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.health',compact('categories'));
    }

    public function locations()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.locations',compact('categories'));
    }

    public function markets()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.markets',compact('categories'));
    }

    public function nursery()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.nursery',compact('categories'));
    }

    public function polling()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.polling',compact('categories'));
    }

    public function primary()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.primary',compact('categories'));
    }

    public function rivers()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.rivers',compact('categories'));
    }

    public function roads()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.roads',compact('categories'));
    }

    public function soils()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('webmaps.soils',compact('categories'));
    }

    public function qgisMaps()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('qgismaps',compact('categories'));
    }

    public function contact()
    {
        $categories = Categories::where('status','=',1)->get();

        return view('contact',compact('categories'));
    }

    public function converterFunction()
    {
        $points = DB::table('emuhaya_users')->get();

        foreach ($points as $point) {
            $marker = DB::table('emuhaya_signup')->where('user_id','=',$point->id)->first();
            if($marker -> active == 'YES')
            {
                DB::table('emuhaya_users')
                    ->where('id', $point->id)
                    ->update(['status' => 1]);
            }
            else{
                continue;
            }
        }

        return 'done';
    }

}
