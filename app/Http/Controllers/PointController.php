<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Points;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PointController extends BaseControler
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = DB::table('emuhaya_categories')
            ->where('status','=',1)
            ->get();

        return view('admin.points.create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        //
        if($validator -> passes()) {

            $Point = new Points();

            $Point->user_id = Auth::user()->id;
            $Point->category_id = $request->input('category');
            $Point->name = $request->input('name');
            $Point->description = $request->input('description');
            $Point->lat = $request->input('latitude');
            $Point->lng = $request->input('longitude');
            $Point->views = 0;
            $Point->likes = 0;
            $Point->path = '';
            $Point->status = 2; // id for a new point
            $Point->created_at = Carbon::now();

            $main_pic = $request->file('image');
            $input['main_pic_name'] = time().'.'.$main_pic->getClientOriginalExtension();


            $main_pic_path = 'uploads/points';
            Image::make($main_pic->getRealPath())
                ->resize(1600, 1200)
                ->save($main_pic_path.'/'.$input['main_pic_name']);

            $mobile_pic_path = 'uploads/points/mobile';
            Image::make($main_pic->getRealPath())
                ->resize(200, 200)
                ->save($mobile_pic_path.'/'.$input['main_pic_name']);

            $web_pic_path = 'uploads/points/web';
            Image::make($main_pic->getRealPath())
                ->resize(400, 400)
                ->save($web_pic_path.'/'.$input['main_pic_name']);

            $Point->image = $input['main_pic_name'];
            $Point->path = $main_pic_path.'/'.$input['main_pic_name'];



            //save order details
            if ($Point->save())
            {
                return Redirect::back()->withSuccess('Point posted successfully!');
            }
            else{
                return Redirect::back()->withErrors('Error when Posting point!');
            }
        }
        else{
            return Redirect::back()->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Point = DB::table('emuhaya_points')
            ->where('id','=',$id)
            ->first();

        $categories = DB::table('emuhaya_categories')
            ->where('status','=',1)
            ->get();

        return view('admin.points.edit',compact('Point','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        //
        if($validator -> passes()) {
            $Point = Points::findOrFail($id);
            $input = $request->all();

            if(!empty($image = $request -> file('file')))
            {
                $main_pic= $request->file('file');
                $input['main_pic_name'] = time().'.'.$main_pic->getClientOriginalExtension();

                $main_pic_path = 'uploads/points';
                $main_pic_img = Image::make($main_pic->getRealPath());
                $main_pic_img->resize(1600, 1200)->save($main_pic_path.'/'.$input['main_pic_name']);

                $mobile_pic_path = 'uploads/points/mobile';
                $mobile_pic_img = Image::make($main_pic->getRealPath());
                $mobile_pic_img->resize(200, 200)->save($mobile_pic_path.'/'.$input['main_pic_name']);

                $web_pic_path = 'uploads/points/web';
                $web_pic_img = Image::make($main_pic->getRealPath());
                $web_pic_img->resize(400, 400)->save($web_pic_path.'/'.$input['main_pic_name']);

                $Point->image = $input['main_pic_name'];
                $Point->path = $main_pic_path.'/'.$input['main_pic_name'];
            }

            if ($Point->fill($input)->save())
            {
                return Redirect::back()->withSuccess('Point Details changes have been updated Successfully');
            }
            else {
                return Redirect::back()->withErrors('Error updating when point details!');
            }
        }
        else{
            return Redirect::back()->withErrors($validator);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Points = Points::findOrFail($id);
        $Points->delete();

        return response()->json(['status' => '00', 'message' => 'Point Details has been Deleted Successfully']);
    }


    public function activate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_points')
            ->where('id', $id)
            ->update(['status' => 1]);

        return response()->json(['status' => '00', 'message' => 'Point has been Activated Successfully']);

    }

    public function deactivate(Request $request){

        $id = $request ->input('id');

        DB::table('emuhaya_points')
            ->where('id', $id)
            ->update(['status' => 2]);

        return response()->json(['status' => '00', 'message' => 'Point has been Deactivated Successfully']);

    }

}
