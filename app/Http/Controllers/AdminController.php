<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Configurations;
use App\Points;
use App\Role;
use App\Staticmaps;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AdminController extends BaseControler
{
    //
    public function index()
    {
        $categories = Categories::all();
        $activeCategories = Categories::where('status','=',1)->get();
        $inactiveCategories = Categories::where('status','<>',1)->get();

        $points = Points::all();
        $activePoints = Points::where('status','=',1)->get();
        $inactivePoints = Points::where('status','<>',1)->get();

        $staticmaps = Staticmaps::all();
        $activeStaticMaps = Staticmaps::where('status','=',1)->get();
        $inactiveStaticMaps = Staticmaps::where('status','<>',1)->get();

        $users = User::all();
        $activeUsers = User::where('status','=',1)->get();
        $inactiveUsers = User::where('status','<>',1)->get();


        return view('admin.index',
            compact(
                'categories',
                'activeCategories',
                'inactiveCategories',
                'points',
                'activePoints',
                'inactivePoints',
                'staticmaps',
                'activeStaticMaps',
                'inactiveStaticMaps',
                'users',
                'activeUsers',
                'inactiveUsers'
            ));
    }

    public function users()
    {
        $roles = Role::all();
        $users = DB::table('emuhaya_users')
            ->paginate(10);
        return view('admin.users.index',compact('roles','users'));
    }

    public function categories()
    {
        $categories = DB::table('emuhaya_categories')
            ->leftjoin('emuhaya_users','emuhaya_categories.user_id','=','emuhaya_users.id')
            ->select('emuhaya_categories.*','emuhaya_users.name as user_name')
            ->paginate(10);

        return view('admin.categories.index',compact('categories'));
    }

    public function points()
    {
        $points = DB::table('emuhaya_points')
            ->leftjoin('emuhaya_categories','emuhaya_points.category_id','=','emuhaya_categories.id')
            ->leftjoin('emuhaya_users','emuhaya_points.user_id','=','emuhaya_users.id')
            ->select('emuhaya_points.*','emuhaya_categories.name as category','emuhaya_users.name as posted_by')
            ->paginate(10);

        return view('admin.points.index',compact('points'));
    }

    public function staticmaps()
    {
        $staticmaps = DB::table('emuhaya_static_maps')
            ->leftjoin('emuhaya_users','emuhaya_static_maps.user_id','=','emuhaya_users.id')
            ->select('emuhaya_static_maps.*','emuhaya_users.name as posted_by')
            ->paginate(10);

        return view('admin.staticmaps.index',compact('staticmaps'));
    }

    public function webmaps()
    {
        $staticmaps = DB::table('emuhaya_static_maps')
            ->leftjoin('emuhaya_users','emuhaya_static_maps.user_id','=','emuhaya_users.id')
            ->select('emuhaya_static_maps.*','emuhaya_users.name as posted_by')
            ->paginate(10);

        return view('admin.staticmaps.index',compact('staticmaps'));
    }

    public function configurations()
    {
        $Configuration = Configurations::find(1);
        return view('admin.configurations.index',compact('Configuration'));
    }

    public function profile()
    {
        $User = Auth::user();
        return view('admin.users.profile',compact('User'));
    }

    public function postProfile(Request $request)
    {

        $User = User::find(Auth::id());

        $User->name = $request->input('name');
        $User->email = $request->input('email');
        $User->phone = $request->input('phone');
        $User->created_at = Carbon::now();

        if(!empty($image = $request -> file('file')))
        {
            $main_pic= $request->file('file');
            $input['main_pic_name'] = time().'.'.$main_pic->getClientOriginalExtension();
            $main_pic_path = 'uploads/profile';
            $main_pic_img = Image::make($main_pic->getRealPath());
            $main_pic_img->resize(170, 170)->save($main_pic_path.'/'.$input['main_pic_name']);

            $User->avatar = $main_pic_path.'/'.$input['main_pic_name'];
        }


        $User->save();


        return back()->with('success','Profile updates successfully!');

    }

}
