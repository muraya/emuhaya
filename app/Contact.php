<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table = 'emuhaya_contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id', 'ip', 'name', 'desc', 'cate_added', 'active', 'likes', 'image', 'posts'];
}
