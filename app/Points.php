<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    //
    protected $table = 'emuhaya_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'category_id', 'name', 'description', 'lat', 'lng', 'approval',
        'views', 'likes', 'image', 'path', 'status', 'created_at'];
}
