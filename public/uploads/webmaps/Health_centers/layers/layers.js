var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_HealthCenters = new ol.format.GeoJSON();
var features_HealthCenters = format_HealthCenters.readFeatures(geojson_HealthCenters, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_HealthCenters = new ol.source.Vector();
jsonSource_HealthCenters.addFeatures(features_HealthCenters);var lyr_HealthCenters = new ol.layer.Vector({
                source:jsonSource_HealthCenters, 
                style: style_HealthCenters,
                title: "Health Centers"
            });var format_Health_Centers = new ol.format.GeoJSON();
var features_Health_Centers = format_Health_Centers.readFeatures(geojson_Health_Centers, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_Health_Centers = new ol.source.Vector();
jsonSource_Health_Centers.addFeatures(features_Health_Centers);var lyr_Health_Centers = new ol.layer.Vector({
                source:jsonSource_Health_Centers, 
                style: style_Health_Centers,
                title: "Health_Centers"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_HealthCenters.setVisible(true);lyr_Health_Centers.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_HealthCenters,lyr_Health_Centers];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_HealthCenters.set('fieldAliases', {'TYPE': 'TYPE', 'IDENT': 'IDENT', 'NAME': 'NAME', 'LAT': 'LAT', 'LONG': 'LONG', });
lyr_Health_Centers.set('fieldAliases', {'Id': 'Id', 'Name': 'Name', 'Type': 'Type', 'Admin': 'Admin', 'Location': 'Location', 'Sublocatn': 'Sublocatn', 'Patien_Day': 'Patien_Day', 'Patien_Yr': 'Patien_Yr', 'Doctors': 'Doctors', 'Med_Off': 'Med_Off', 'Nurses': 'Nurses', 'Wards': 'Wards', 'Yr_Establ': 'Yr_Establ', 'Facilities': 'Facilities', 'Bed_Capcty': 'Bed_Capcty', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_HealthCenters.set('fieldImages', {'TYPE': 'TextEdit', 'IDENT': 'TextEdit', 'NAME': 'TextEdit', 'LAT': 'TextEdit', 'LONG': 'TextEdit', });
lyr_Health_Centers.set('fieldImages', {'Id': 'TextEdit', 'Name': 'TextEdit', 'Type': 'TextEdit', 'Admin': 'TextEdit', 'Location': 'TextEdit', 'Sublocatn': 'TextEdit', 'Patien_Day': 'TextEdit', 'Patien_Yr': 'TextEdit', 'Doctors': 'TextEdit', 'Med_Off': 'TextEdit', 'Nurses': 'TextEdit', 'Wards': 'TextEdit', 'Yr_Establ': 'TextEdit', 'Facilities': 'TextEdit', 'Bed_Capcty': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_HealthCenters.set('fieldLabels', {'TYPE': 'no label', 'IDENT': 'no label', 'NAME': 'no label', 'LAT': 'no label', 'LONG': 'no label', });
lyr_Health_Centers.set('fieldLabels', {'Id': 'no label', 'Name': 'no label', 'Type': 'no label', 'Admin': 'no label', 'Location': 'no label', 'Sublocatn': 'no label', 'Patien_Day': 'no label', 'Patien_Yr': 'no label', 'Doctors': 'no label', 'Med_Off': 'no label', 'Nurses': 'no label', 'Wards': 'no label', 'Yr_Establ': 'no label', 'Facilities': 'no label', 'Bed_Capcty': 'no label', });
lyr_Health_Centers.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});