var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_Markets = new ol.format.GeoJSON();
var features_Markets = format_Markets.readFeatures(geojson_Markets, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_Markets = new ol.source.Vector();
jsonSource_Markets.addFeatures(features_Markets);var lyr_Markets = new ol.layer.Vector({
                source:jsonSource_Markets, 
                style: style_Markets,
                title: "Markets"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_Markets.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_Markets];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_Markets.set('fieldAliases', {'Id': 'Id', 'Name': 'Name', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_Markets.set('fieldImages', {'Id': 'TextEdit', 'Name': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_Markets.set('fieldLabels', {'Id': 'no label', 'Name': 'no label', });
lyr_Markets.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});