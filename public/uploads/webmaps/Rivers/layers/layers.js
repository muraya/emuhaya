var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_new_emuhaya_rivers_36n = new ol.format.GeoJSON();
var features_new_emuhaya_rivers_36n = format_new_emuhaya_rivers_36n.readFeatures(geojson_new_emuhaya_rivers_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_rivers_36n = new ol.source.Vector();
jsonSource_new_emuhaya_rivers_36n.addFeatures(features_new_emuhaya_rivers_36n);var lyr_new_emuhaya_rivers_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_rivers_36n, 
                style: style_new_emuhaya_rivers_36n,
                title: "new_emuhaya_rivers_36n"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_new_emuhaya_rivers_36n.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_new_emuhaya_rivers_36n];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_new_emuhaya_rivers_36n.set('fieldAliases', {'DGC_CODICE': 'DGC_CODICE', 'TYPE': 'TYPE', 'NAME': 'NAME', 'CODE': 'CODE', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_new_emuhaya_rivers_36n.set('fieldImages', {'DGC_CODICE': 'TextEdit', 'TYPE': 'TextEdit', 'NAME': 'TextEdit', 'CODE': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_new_emuhaya_rivers_36n.set('fieldLabels', {'DGC_CODICE': 'no label', 'TYPE': 'no label', 'NAME': 'no label', 'CODE': 'no label', });
lyr_new_emuhaya_rivers_36n.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});