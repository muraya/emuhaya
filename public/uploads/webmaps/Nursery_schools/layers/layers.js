var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_nursery_schools = new ol.format.GeoJSON();
var features_nursery_schools = format_nursery_schools.readFeatures(geojson_nursery_schools, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_nursery_schools = new ol.source.Vector();
jsonSource_nursery_schools.addFeatures(features_nursery_schools);var lyr_nursery_schools = new ol.layer.Vector({
                source:jsonSource_nursery_schools, 
                style: style_nursery_schools,
                title: "nursery_schools"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_nursery_schools.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_nursery_schools];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_nursery_schools.set('fieldAliases', {'ID': 'ID', 'STN_NO': 'STN_NO', 'STATUS': 'STATUS', 'STN_NAME': 'STN_NAME', 'LOCATION': 'LOCATION', 'DIVISION': 'DIVISION', 'DISTRICT': 'DISTRICT', 'ELEC_AREA_': 'ELEC_AREA_', 'ELEC_AREA1': 'ELEC_AREA1', 'LOCAL_AUTH': 'LOCAL_AUTH', 'CONST_NO': 'CONST_NO', 'CONST_NAM': 'CONST_NAM', 'SUBLOCATIO': 'SUBLOCATIO', 'SOURCETHM': 'SOURCETHM', 'PROVINCE': 'PROVINCE', 'REMARKS': 'REMARKS', 'DATA_SOURC': 'DATA_SOURC', 'TYPE': 'TYPE', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_nursery_schools.set('fieldImages', {'ID': 'TextEdit', 'STN_NO': 'TextEdit', 'STATUS': 'TextEdit', 'STN_NAME': 'TextEdit', 'LOCATION': 'TextEdit', 'DIVISION': 'TextEdit', 'DISTRICT': 'TextEdit', 'ELEC_AREA_': 'TextEdit', 'ELEC_AREA1': 'TextEdit', 'LOCAL_AUTH': 'TextEdit', 'CONST_NO': 'TextEdit', 'CONST_NAM': 'TextEdit', 'SUBLOCATIO': 'TextEdit', 'SOURCETHM': 'TextEdit', 'PROVINCE': 'TextEdit', 'REMARKS': 'TextEdit', 'DATA_SOURC': 'TextEdit', 'TYPE': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_nursery_schools.set('fieldLabels', {'ID': 'no label', 'STN_NO': 'no label', 'STATUS': 'no label', 'STN_NAME': 'no label', 'LOCATION': 'no label', 'DIVISION': 'no label', 'DISTRICT': 'no label', 'ELEC_AREA_': 'no label', 'ELEC_AREA1': 'no label', 'LOCAL_AUTH': 'no label', 'CONST_NO': 'no label', 'CONST_NAM': 'no label', 'SUBLOCATIO': 'no label', 'SOURCETHM': 'no label', 'PROVINCE': 'no label', 'REMARKS': 'no label', 'DATA_SOURC': 'no label', 'TYPE': 'no label', });
lyr_nursery_schools.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});