var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_ContoursClip = new ol.format.GeoJSON();
var features_ContoursClip = format_ContoursClip.readFeatures(geojson_ContoursClip, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_ContoursClip = new ol.source.Vector();
jsonSource_ContoursClip.addFeatures(features_ContoursClip);var lyr_ContoursClip = new ol.layer.Vector({
                source:jsonSource_ContoursClip, 
                style: style_ContoursClip,
                title: "Contours Clip"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_ContoursClip.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_ContoursClip];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_ContoursClip.set('fieldAliases', {'CONTOUR': 'CONTOUR', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_ContoursClip.set('fieldImages', {'CONTOUR': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_ContoursClip.set('fieldLabels', {'CONTOUR': 'no label', });
lyr_ContoursClip.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});