var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_Bridges = new ol.format.GeoJSON();
var features_Bridges = format_Bridges.readFeatures(geojson_Bridges, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_Bridges = new ol.source.Vector();
jsonSource_Bridges.addFeatures(features_Bridges);var lyr_Bridges = new ol.layer.Vector({
                source:jsonSource_Bridges, 
                style: style_Bridges,
                title: "Bridges"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_Bridges.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_Bridges];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_Bridges.set('fieldAliases', {'TYPE': 'TYPE', 'IDENT': 'IDENT', 'NAME': 'NAME', 'LAT': 'LAT', 'LONG': 'LONG', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_Bridges.set('fieldImages', {'TYPE': 'TextEdit', 'IDENT': 'TextEdit', 'NAME': 'TextEdit', 'LAT': 'TextEdit', 'LONG': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_Bridges.set('fieldLabels', {'TYPE': 'no label', 'IDENT': 'no label', 'NAME': 'no label', 'LAT': 'no label', 'LONG': 'no label', });
lyr_Bridges.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});