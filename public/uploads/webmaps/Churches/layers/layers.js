var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_Churches = new ol.format.GeoJSON();
var features_Churches = format_Churches.readFeatures(geojson_Churches, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_Churches = new ol.source.Vector();
jsonSource_Churches.addFeatures(features_Churches);var lyr_Churches = new ol.layer.Vector({
                source:jsonSource_Churches, 
                style: style_Churches,
                title: "Churches"
            });var format_Churches_point = new ol.format.GeoJSON();
var features_Churches_point = format_Churches_point.readFeatures(geojson_Churches_point, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_Churches_point = new ol.source.Vector();
jsonSource_Churches_point.addFeatures(features_Churches_point);var lyr_Churches_point = new ol.layer.Vector({
                source:jsonSource_Churches_point, 
                style: style_Churches_point,
                title: "Churches_point"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_Churches.setVisible(true);lyr_Churches_point.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_Churches,lyr_Churches_point];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_Churches.set('fieldAliases', {'Id': 'Id', 'Name': 'Name', 'Denminatn': 'Denminatn', 'Type': 'Type', 'Location': 'Location', 'Sub_Locatn': 'Sub_Locatn', });
lyr_Churches_point.set('fieldAliases', {'TYPE': 'TYPE', 'IDENT': 'IDENT', 'NAME': 'NAME', 'LAT': 'LAT', 'LONG': 'LONG', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_Churches.set('fieldImages', {'Id': 'TextEdit', 'Name': 'TextEdit', 'Denminatn': 'TextEdit', 'Type': 'TextEdit', 'Location': 'TextEdit', 'Sub_Locatn': 'TextEdit', });
lyr_Churches_point.set('fieldImages', {'TYPE': 'TextEdit', 'IDENT': 'TextEdit', 'NAME': 'TextEdit', 'LAT': 'TextEdit', 'LONG': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_Churches.set('fieldLabels', {'Id': 'no label', 'Name': 'no label', 'Denminatn': 'no label', 'Type': 'no label', 'Location': 'no label', 'Sub_Locatn': 'no label', });
lyr_Churches_point.set('fieldLabels', {'TYPE': 'no label', 'IDENT': 'no label', 'NAME': 'no label', 'LAT': 'no label', 'LONG': 'no label', });
lyr_Churches_point.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});