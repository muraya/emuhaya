var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_Buildings = new ol.format.GeoJSON();
var features_Buildings = format_Buildings.readFeatures(geojson_Buildings, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_Buildings = new ol.source.Vector();
jsonSource_Buildings.addFeatures(features_Buildings);var lyr_Buildings = new ol.layer.Vector({
                source:jsonSource_Buildings, 
                style: style_Buildings,
                title: "Buildings"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_Buildings.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_Buildings];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_Buildings.set('fieldAliases', {'Id': 'Id', 'Obsvtion': 'Obsvtion', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_Buildings.set('fieldImages', {'Id': 'TextEdit', 'Obsvtion': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_Buildings.set('fieldLabels', {'Id': 'no label', 'Obsvtion': 'no label', });
lyr_Buildings.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});