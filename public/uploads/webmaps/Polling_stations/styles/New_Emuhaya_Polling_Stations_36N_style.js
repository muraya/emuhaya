var size = 0;

var styleCache_New_Emuhaya_Polling_Stations_36N={}
var style_New_Emuhaya_Polling_Stations_36N = function(feature, resolution){
    var value = ""
    var size = 0;
    var style = [ new ol.style.Style({
        image: new ol.style.Circle({radius: 4.0 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}), fill: new ol.style.Fill({color: 'rgba(229,164,211,1.0)'})})
    })];
    if ("" !== null) {
        var labelText = String("");
    } else {
        var labelText = ""
    }
    var key = value + "_" + labelText

    if (!styleCache_New_Emuhaya_Polling_Stations_36N[key]){
        var text = new ol.style.Text({
              font: '10px \'None\', sans-serif',
              text: labelText,
              textBaseline: "center",
              textAlign: "left",
              offsetX: 5,
              offsetY: 3,
              fill: new ol.style.Fill({
                color: 'rgba(None, None, None, 255)'
              }),
            });
        styleCache_New_Emuhaya_Polling_Stations_36N[key] = new ol.style.Style({"text": text})
    }
    var allStyles = [styleCache_New_Emuhaya_Polling_Stations_36N[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};