var baseLayer = new ol.layer.Group({
    'title': 'Base maps',
    layers: [
new ol.layer.Tile({
    'title': 'OSM',
    'type': 'base',
    source: new ol.source.OSM()
})
]
});
var format_new_emuhaya_constituency_36n = new ol.format.GeoJSON();
var features_new_emuhaya_constituency_36n = format_new_emuhaya_constituency_36n.readFeatures(geojson_new_emuhaya_constituency_36n, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_new_emuhaya_constituency_36n = new ol.source.Vector();
jsonSource_new_emuhaya_constituency_36n.addFeatures(features_new_emuhaya_constituency_36n);var lyr_new_emuhaya_constituency_36n = new ol.layer.Vector({
                source:jsonSource_new_emuhaya_constituency_36n, 
                style: style_new_emuhaya_constituency_36n,
                title: "new_emuhaya_constituency_36n"
            });var format_MajorRoads = new ol.format.GeoJSON();
var features_MajorRoads = format_MajorRoads.readFeatures(geojson_MajorRoads, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:32636'});
var jsonSource_MajorRoads = new ol.source.Vector();
jsonSource_MajorRoads.addFeatures(features_MajorRoads);var lyr_MajorRoads = new ol.layer.Vector({
                source:jsonSource_MajorRoads, 
                style: style_MajorRoads,
                title: "Major Roads"
            });

lyr_new_emuhaya_constituency_36n.setVisible(true);lyr_MajorRoads.setVisible(true);
var layersList = [baseLayer,lyr_new_emuhaya_constituency_36n,lyr_MajorRoads];
lyr_new_emuhaya_constituency_36n.set('fieldAliases', {'DISTRICT': 'DISTRICT', 'COUNT': 'COUNT', 'Area': 'Area', });
lyr_MajorRoads.set('fieldAliases', {'ID': 'ID', 'TYPE': 'TYPE', 'SURFACE': 'SURFACE', 'Road_Type': 'Road_Type', 'DIST_KM': 'DIST_KM', });
lyr_new_emuhaya_constituency_36n.set('fieldImages', {'DISTRICT': 'TextEdit', 'COUNT': 'TextEdit', 'Area': 'TextEdit', });
lyr_MajorRoads.set('fieldImages', {'ID': 'TextEdit', 'TYPE': 'TextEdit', 'SURFACE': 'TextEdit', 'Road_Type': 'TextEdit', 'DIST_KM': 'TextEdit', });
lyr_new_emuhaya_constituency_36n.set('fieldLabels', {'DISTRICT': 'no label', 'COUNT': 'no label', 'Area': 'no label', });
lyr_MajorRoads.set('fieldLabels', {'ID': 'no label', 'TYPE': 'no label', 'SURFACE': 'no label', 'Road_Type': 'no label', 'DIST_KM': 'no label', });
lyr_MajorRoads.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});