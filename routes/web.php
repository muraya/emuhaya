<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/', 'PageController@index')->name('index');
Route::get('mapped', 'PageController@mapped');
Route::get('listed', 'PageController@listed');
Route::get('listed/details/{id}', 'PageController@listedDetails');
Route::get('category', 'PageController@category');
Route::get('staticmap', 'PageController@staticMap');
Route::get('staticmap/{id}', 'PageController@staticMap');
Route::get('qgismaps', 'PageController@qgisMaps');
Route::get('webmaps', 'PageController@webmaps');
Route::get('webmaps/bridges', 'PageController@bridges');
Route::get('webmaps/building', 'PageController@building');
Route::get('webmaps/churches', 'PageController@churches');
Route::get('webmaps/contours', 'PageController@contours');
Route::get('webmaps/health', 'PageController@health');
Route::get('webmaps/locations', 'PageController@locations');
Route::get('webmaps/markets', 'PageController@markets');
Route::get('webmaps/nursery', 'PageController@nursery');
Route::get('webmaps/polling', 'PageController@polling');
Route::get('webmaps/primary', 'PageController@primary');
Route::get('webmaps/rivers', 'PageController@rivers');
Route::get('webmaps/roads', 'PageController@roads');
Route::get('webmaps/soils', 'PageController@soils');
Route::get('contact', 'PageController@contact');


Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::get('admin', 'AdminController@index');
    Route::get('admin/roles', 'AdminController@roles');
    Route::get('admin/users', 'AdminController@users');
    Route::get('admin/categories', 'AdminController@categories');
    Route::get('admin/points', 'AdminController@points');
    Route::get('admin/staticmaps', 'AdminController@staticmaps');
    Route::get('admin/logins', 'AdminController@logins');
    Route::get('admin/configurations', 'AdminController@configurations');
    Route::get('admin/profile', 'AdminController@profile');
    Route::post('admin/profile', 'AdminController@postProfile');

});

Route::get('user', 'UserController@index');

Route::resource('usersData', 'UsersController@usersData');
Route::post('users/activate', 'UsersController@activate');
Route::post('users/deactivate', 'UsersController@deactivate');
Route::resource('users', 'UsersController');

Route::post('categories/activate', 'CategoryController@activate');
Route::post('categories/deactivate', 'CategoryController@deactivate');
Route::resource('categories', 'CategoryController');

Route::post('points/activate', 'PointController@activate');
Route::post('points/deactivate', 'PointController@deactivate');
Route::resource('points', 'PointController');

Route::post('staticmaps/activate', 'StaticmapsController@activate');
Route::post('staticmaps/deactivate', 'StaticmapsController@deactivate');
Route::resource('staticmaps', 'StaticmapsController');

Route::resource('configurations', 'ConfigurationController');

Route::get('sparkpost', function () {
    Mail::send('emails.test', [], function ($message) {
        $message
            ->from('mail@emuhayawebgis.info', 'Emuhaya Web Portal')
      ->to('dancmuraya10@gmail.com', 'Muraya Duncan')
      ->subject('From SparkPost with ❤');
  });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('converterfunction', 'PageController@converterFunction');

