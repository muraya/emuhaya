<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//<editor-fold desc="API Routes for mobile application">
//Route::post('test', 'ApiController@login');

Route::post('login', 'ApiController@login');

Route::post('register', 'ApiController@register');

Route::post('reset', 'ApiController@reset');

Route::post('update', 'ApiController@update');

Route::get('points', 'ApiController@getPoints');

Route::post('points', 'ApiController@postPoint');

Route::post('userpoints', 'ApiController@getUserPoints');



Route::get('users', 'ApiController@getUsers');

Route::get('categories', 'ApiController@getCategories');
//</editor-fold>

//<editor-fold desc="API Routes for web application requests">
Route::get('webpoints', 'ApiController@getWebPoints');

Route::get('webpointdetails', 'ApiController@webPointDetails');
//</editor-fold>


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
