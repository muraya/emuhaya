<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/15/2017
 * Time: 3:24 PM
 */

?>

@extends('layouts.index')
@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container-fluid">
                        <h1> QGIS Web Maps</h1>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.page-title -->
                <div class="container-fluid">
                    <div class="row" id="siteloader">

                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

    <script>

    </script>

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#siteloader").html('<object style="height: 700px; width: 100%"  data="http://qgiscloud.com/fananda/Emuhaya_shapefiles2">');

        });
    </script>
@endsection
