<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/1/2017
 * Time: 11:38 AM
 */

?>

@extends('layouts.index')
@section('content')
    <div class="container mt80">
        <div class="row">
            {{ $categories->links() }}
        </div>
        <div class="row">
            {{--<div class="col-lg-12">--}}
                <!-- /.sort-options -->
                @foreach($categories as $category)
                    <div class="col-md-6">
                        <div class="listing-row">
                            <div class="listing-row-inner">
                                <a class="listing-row-image" href="#">
                                    <span class="listing-row-image-content" style="background-image: url({{ asset('uploads/categories/'.$category->image) }})"></span>
                                </a>
                                <div class="listing-row-content">
                                    <div class="listing-row-content-header">
                                        <h3><a href="#">{{ $category->name }}</a></h3>
                                        <div class="actions">
                                            <a href="#" id="point_id" data-id=""><i class="fa fa-heart fa-2x"></i></a>
                                        </div>
                                        <!-- /.actions -->
                                    </div>
                                    <!-- /.listing-row-content-header -->
                                    <div class="listing-row-content-meta">
                                        <!-- /.listing-row-meta-item -->
                                        <div class="listing-row-content-meta-item listing-row-content-meta-category">
                                            <span class="tag tag-black">{{ strtok($category->name, " ") }}</span>
                                        </div>
                                        <div class="listing-row-content-meta-item listing-row-content-meta-category">
                                            <span class="tag">{{ $category->posts }}</span>
                                        </div>
                                        <!-- /.listing-row-meta-item -->
                                        <!-- /.listing-row-meta-item -->
                                    </div>
                                    <!-- /.listing-row-meta-item -->
                                    <div class="listing-row-content-body">
                                    {{ substr($category->description, 0, 190) }}
                                    <!-- /.listing-row-content-read-more -->
                                    </div>
                                    <!-- /.listing-row-content-body -->
                                </div>
                                <!-- /.listing-row-content -->
                            </div>
                            <!-- /.listing-row-inner -->
                        </div>
                    </div>
                @endforeach

            {{--</div>--}}
            <!-- /.col -->
        </div>
    {{ $categories->links() }}
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
