<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 18-Jan-18
 * Time: 19:53
 */

?>


@extends('layouts.index')
@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container">
                        <h1> Polling Stations </h1>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.page-title -->
                <div class="container-fluid">
                    <div class="row">

                    @include('partials.webmaps')

                    <!-- /.col-* -->
                        <div class="col-sm-9 col-md-10">
                            <div class="row" id="siteloader">

                            </div>
                            <!-- /.content -->
                        </div>
                        <!-- /.col-* -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#siteloader").html('<object style="height: 700px; width: 100%"  data="{{ asset('uploads/webmaps/Polling_stations/index.html') }}">');

        });
    </script>
@endsection
