<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/2/2017
 * Time: 9:07 AM
 */
?>


<script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&amp;key=AIzaSyA8SyyT4D-TMLy-UpymLHVxUcX67ATxFHQ" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tether.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/chartist.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/google-map-richmarker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/google-map-infobox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/google-map-markerclusterer.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/google-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.trackpad-scroll-emulator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.inlinesvg.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.affix.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.scrollTo.js') }}"></script>
<script type="text/javascript" src="{{ asset('libraries/slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/nouislider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/wNumb.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/particles.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/explorer.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/explorer-map-search.js') }}"></script>