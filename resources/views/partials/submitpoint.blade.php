<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 9:41 PM
 */
?>


<div id="modal-submit" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Submit New Listing</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('points') }}" class="mb80" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <fieldset>
                        <legend>Basic Information</legend>
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="category"> Category</label>
                            <select class="form-control" name="category" id="category">
                                <option value=""> --  Select One --</option>
                                @foreach($categories as $category)
                                    <option value="{!! $category -> id !!}"> {!! $category -> name !!} </option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="6"></textarea>
                        </div>
                        <!-- /.form-group -->
                    </fieldset>
                    <fieldset>
                        <legend>Location</legend>
                        <div class="row">
                            <div class="col-sm-8">
                                <div id="location-google-map" style="height: 450px;"></div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text" name="location" id="location" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input type="file" name="image" id="image" class="form-control location-google-map-search">
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label for="listing_location_latitude">Latitude</label>
                                    <input type="text" name="latitude" class="form-control" id="listing_location_latitude">
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label for="listing_location_longitude">Longitude</label>
                                    <input type="text" name="longitude" class="form-control" id="listing_location_longitude">
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </fieldset>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit Point</button>
            </div>
            </form>
        </div>
    </div>
</div>
