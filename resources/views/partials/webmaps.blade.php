<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 18-Jan-18
 * Time: 01:30
 */
?>

<div class="col-sm-3 col-md-2 sidebar-wrapper-col">
    <div class="sidebar">
        <div class="filter">
            <h2>Search criteria</h2>
            <!-- /.form-group -->
            <!-- /.checkbox -->
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps') }}">Administration Centres</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/bridges') }}">Bridges</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/building') }}">Buildings</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/churches') }}">Churches</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/contours') }}">Contours</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/health') }}">Health Centres</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/locations') }}">Locations</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/markets') }}">Markets</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/nursery') }}">Nursery Schools</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/polling') }}">Polling Stations</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/primary') }}">Primary Schools</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/rivers') }}"> Rivers</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/roads') }}">Roads</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <a href="{{ url('webmaps/soils') }}">Soils</a>
                </label>
            </div>
            <!-- /.checkbox -->
            <div class="form-group-btn form-group-btn-placeholder-gap">
                <button type="submit" class="btn btn-primary btn-block">Search</button>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.filter -->
    </div>
    <!-- /.sidebar -->
</div>

