<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 9:37 PM
 */
?>


<div class="header-wrapper">
    <div class="header">
        <div class="container">
            <div class="header-inner">
                <div class="navigation-toggle toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- /.header-toggle -->
                <div class="header-logo">
                    <a href="{{ url('') }}">
                        <img src="{{ asset('img/logo.svg') }}" class="svg" alt="Home">
                    </a>
                    <a href="{{ url('') }}" class="header-title">Emuhaya</a>
                </div>
                <!-- /.header-logo -->
                <div class="header-nav">
                    <div class="primary-nav-wrapper">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="{{ url('') }}" class="nav-link active">Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('mapped') }}" class="nav-link ">Culture Mapping</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('listed') }}" class="nav-link ">Culture Listing</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('category') }}" class="nav-link ">Categories</a>
                            </li>
                            <li class="nav-item has-sub-menu">
                                <a href="#" class="nav-link ">Maps</a>
                                <ul class="sub-menu">
                                    <li><a href="{{ url('staticmap') }}">Static Maps</a></li>
                                    <li><a href="{{ url('qgismaps') }}">QGIS Maps</a></li>
                                    <li><a href="{{ url('webmaps') }}">Web Maps</a></li>
                                </ul>
                            </li>
                            <li class="nav-item ">
                                <a href="{{ url('contact') }}" class="nav-link ">Reach Us</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.primary-nav-wrapper -->
                </div>
                @if(Auth::check())
                    <a href="#" title="Log Out" onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">
                        <div class="toggle">
                            <i class="fa fa-sign-out fa-2x"></i>
                        </div>
                    </a>
                        <form id="logout-form"
                              action="{{ url('/logout') }}"
                              method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                 @else
                    <a href="{{ url('login') }}">
                        <div class="toggle">
                            <i class="fa fa-lock fa-2x"></i>
                        </div>
                    </a>
                @endif
                <!-- /.header-toggle -->
                @if(Auth::check())
                    <div class="header-actions">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a href="#" class="nav-link modal-submit" id="modal-action-submit" data-toggle="modal" data-target="#modal-submit">
                                    <i class="fa fa-plus"></i> <span>Submit Listing</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                @else
                    <div class="header-actions">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a href="{{ url('login') }}" class="nav-link modal-submit">
                                    <i class="fa fa-plus"></i> <span>Login to Submit Listing</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                @endif
                <!-- /.header-actions -->
            </div>
            <!-- /.header-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header -->
</div>
