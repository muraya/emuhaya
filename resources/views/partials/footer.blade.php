<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 9:38 PM
 */

?>

<div class="footer-wrapper">
    <div class="footer">
        <div class="footer-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="widget">
                            <h3>
                                <img src="{{ asset('img/logo.svg') }}" class="svg" alt="Home">
                                Emuhaya Web Portal
                                <span></span>
                            </h3>
                            <p>
                                Here you can view and submit distinctive points within our region
                            </p>
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.col-* -->
                    <div class="col-lg-4">
                        <div class="widget">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link" href="{{ url('') }}">Home </a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('mapped') }}">Culture Mapping</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('listed') }}">Culture Listing</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('staticmap') }}">Static Maps</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('webmaps') }}">Web Maps</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ url('contact') }}">Contact Us</a></li>
                            </ul>
                        </div>
                        <!-- /.widget -->
                    </div>
                    <!-- /.col-* -->
                    <div class="col-lg-4">
                        <div class="widget">
                            <h3>Best deals in your inbox</h3>
                            <p>
                                Join our newsletter for the most recent information.
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group clearfix mt40">
                                        <a href="{{ url('admin') }}" class="btn btn-primary pull-right">Admin</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <form method="post" action="{{ url('contacts') }}">
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Your e-mail address">
                                            @if(Auth::check() && Auth::user()->role_id == 1)
                                                <a href="{{ url('admin') }}" title="Access Admin Account">
                                                    <div class="toggle">
                                                        <i class="fa fa-user fa-2x"></i>
                                                    </div>
                                                </a>
                                            @endif
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group clearfix">
                                            <button type="submit" class="btn btn-primary pull-right">Join Newsletter</button>
                                        </div>
                                        <!-- /.form-group -->
                                    </form>
                                </div>
                            </div>



                        </div>
                        <!-- /.widget -->
                    </div>
                    <!-- /.col-* -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.footer-inner -->
    </div>
    <!-- /.footer -->
</div>
