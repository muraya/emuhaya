<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/13/2017
 * Time: 11:39 AM
 */
?>

<div id="modal-login" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login Here First</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="login-form-email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="login-form-email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="login-form-password">Password</label>
                        <input type="password" class="form-control" name="password" id="login-form-password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <!-- /.form-group -->
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                    <button type="submit" class="btn btn-primary pull-right">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>

