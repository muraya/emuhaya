<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/2/2017
 * Time: 9:06 AM
 */
?>

<link href="{{ asset('libraries/slick/slick.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('libraries/slick/slick-theme.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/trackpad-scroll-emulator.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/chartist.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/jquery.raty.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/nouislider.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/explorer.css') }}" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png') }}">
