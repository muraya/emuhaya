<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 9:40 PM
 */

?>

<div class="side-wrapper">
    <div class="side">
        @if(Auth::check())
            <a href="{{url('profile')}}" class="nav-link"><strong> My Account </strong></a>
            <div class="side-inner">
                <div class="side-user">
                    <span class="avatar" style="background-image: url({{ asset('uploads/points/web/'.Auth::user()->avatar) }});"></span>
                    <span class="side-user-avatar-name">
                      <strong>{!! Auth::user()->name !!}</strong>
                      <span>{!! Auth::user()->role !!}</span>
                    </span>
                    <a href="#" class="side-user-avatar-action">
                        <i class="fa fa-sign-out fa-2x"></i>
                    </a>
                </div>
                <!-- /.side-user -->
                <ul class="nav flex-column">
                    @if(Auth::user()->role_id == 1)
                        <li class="nav-item"><a href="{{ url('admin') }}" class="nav-link">Administrator Panel</a></li>
                    @endif
                </ul>
                <h3>Welcome Back</h3>
            </div>
        @else
            <div class="side-inner">
                <div class="side-user">
                    <span class="side-user-avatar-name">
                      <strong>Login Here</strong>
                    </span>
                    <a href="#" class="side-user-avatar-action">
                        <i class="fa fa-sign-in fa-2x"></i>
                    </a>
                </div>
                <!-- /.side-user -->
                <h3>Login Here</h3>
                @include('admin.partials.flash-message')
                <form class="form-dark" method="post" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- /.form-group -->
                    <button class="btn btn-primary pull-right" type="submit">Login</button>
                </form>
            </div>

    @endif
        <!-- /.side-inner -->
    </div>
    <!-- /.side -->
</div>
