<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 11:26 PM
 */

 ?>

@extends('layouts.index')
@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container-fluid">
                        <h1> {!! $StaticMap -> name !!}</h1>
                        {{--<div class="page-title-actions">--}}
                            {{--<div class="switcher">--}}
                                {{--<button class="btn btn-sm">Print PDF</button>--}}
                            {{--</div>--}}
                            {{--<!-- /.switcher -->--}}
                            {{--<div class="switcher">--}}
                                {{--<button class="btn btn-sm">Download</button>--}}
                            {{--</div>--}}
                            {{--<!-- /.switcher -->--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.page-title -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3 col-md-2 sidebar-wrapper-col">
                            <div class="sidebar">
                                <div class="filter">
                                    <h2>Search criteria</h2>
                                    <!-- /.form-group -->
                                    <!-- /.checkbox -->
                                    @foreach($static_maps as $static_map)
                                    <div class="checkbox">
                                        <label>
                                            <a href="{{ url('staticmap/'.$static_map -> id) }}" >{!! $static_map -> name !!}</a>
                                        </label>
                                    </div>
                                    @endforeach
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.filter -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                        <!-- /.col-* -->
                        <div class="col-sm-9 col-md-10">
                            <div class="content">
                                <img src="{{ asset('uploads/staticmaps/'.$StaticMap->image) }}">
                            </div>
                            <!-- /.content -->
                        </div>
                        <!-- /.col-* -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->
@endsection
