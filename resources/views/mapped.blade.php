<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 11:25 PM
 */
?>

@extends('layouts.index')
@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="map-wrapper">
                        <div class="map">
                            <div class="map-inner">
                                <div class="map-results">
                                    <div class="map-results-inner">
                                        <div class="map-results-list tse-scrollable">
                                            <div class="tse-content">
                                                <div class="map-results-header">
                                                    <h3>Mapped Culture </h3>
                                                    <div class="map-results-count">{!! count($points) !!}</div>
                                                    <!-- /.map-results-count -->
                                                </div>
                                                <!-- /.map-results-header -->
                                                <div class="map-results-content clickable">
                                                    @foreach($points as $point)
                                                        <div class="listing-row-medium" data-id="{!! $point -> id !!}">
                                                            <div class="listing-row-medium-inner">
                                                                <div class="listing-row-medium-image">
                                                                    <img src="{{ asset('uploads/points/web/'.$point->image) }}" alt="{!! $point -> name !!}">
                                                                </div>
                                                                <div class="listing-row-medium-content">
                                                                    <a class="listing-row-medium-category tag tag-black" href="{{ url('point/details/'.$point->id) }}">{!! $point -> category !!}</a>
                                                                    <span class="listing-row-medium-rating">
                                                                  <span>{!! $point -> approval !!}</span>
                                                                </span>
                                                                    <h4 class="listing-row-medium-title"><a href="{{ url('point/details/'.$point->id) }}">{!! $point -> name !!}</a></h4>
                                                                    <div class="listing-row-medium-address">
                                                                        <i class="fa fa-map-marker"></i>  {!! $point -> lat !!} - {!! $point -> lng !!}
                                                                    </div>
                                                                    <!-- /.listing-row-medium-address -->
                                                                </div>
                                                                <!-- /.listing-row-medium-content -->
                                                            </div>
                                                            <!-- /.listing-row-medium-inner -->
                                                        </div>
                                                    @endforeach
                                                    <!-- /.listings-row-medium -->
                                                </div>
                                                <!-- /.map-results-content -->
                                            </div>
                                            <!-- /.tse-content -->
                                        </div>
                                        <!-- /.map-results-list -->
                                        <div class="map-results-detail tse-scrollable">
                                            <div class="tse-content">
                                                <div class="map-results-detail-header">
                                                    <h3 id="point_name"></h3>
                                                    <div class="map-results-detail-address" id="point_address">

                                                    </div>
                                                    <!-- /.map-results-detail-address -->
                                                    <div class="actions">
                                                      <a href="#" id="point_id" data-id=""><i class="fa fa-heart fa-2x"></i></a>
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- .map-results-detail-header -->
                                                <h4>Photo</h4>
                                                <img id="point_image" src="" alt="">
                                                <!-- /.map-results-detail-gallery -->
                                                <h4>Description</h4>
                                                <p>
                                                    <strong id="point_description"></strong>
                                                </p>
                                            </div>
                                            <!-- /.tse-content -->
                                        </div>
                                        <!-- /.map-results-detail -->
                                        <div class="map-results-toggle">
                                            <i class="fa fa-chevron-left"></i>
                                        </div>
                                        <!-- /.map-results-toggle -->
                                    </div>
                                    <!-- /.map-results-inner -->
                                </div>
                                <!-- /.map-results -->
                                <div class="map-object">
                                    <div id="map-object"></div>
                                    <div class="map-toolbar">
                                        <div class="map-toolbar-group">
                                            <div id="map-toolbar-action-zoom-in" class="map-toolbar-group-item"><i class="fa fa-plus"></i></div>
                                            <!-- /.map-toolbar-group-item -->
                                            <div id="map-toolbar-action-zoom-out" class="map-toolbar-group-item"><i class="fa fa-minus"></i></div>
                                            <!-- /.map-toolbar-group-item -->
                                        </div>
                                        <!-- /.map-toolbar-group -->
                                        <div class="map-toolbar-group">
                                            <div id="map-toolbar-action-current-position" class="map-toolbar-group-item"><i class="fa fa-location-arrow"></i></div>
                                            <!-- /.map-toolbar-group-item -->
                                            <div id="map-toolbar-action-fullscreen" class="map-toolbar-group-item"><i class="fa fa-arrows-alt"></i></div>
                                            <!-- /.map-toolbar-group-item -->
                                        </div>
                                        <!-- /.map-toolbar-group -->
                                        <div class="map-toolbar-group">
                                            <div id="map-toolbar-action-roadmap" class="map-toolbar-group-item">Roadmap</div>
                                            <!-- /.map-toolbar-group-item -->
                                            <div id="map-toolbar-action-satellite" class="map-toolbar-group-item">Satellite</div>
                                            <!-- /.map-toolbar-group-item -->
                                            <div id="map-toolbar-action-terrain" class="map-toolbar-group-item">Terrain</div>
                                            <!-- /.map-toolbar-group-item -->
                                        </div>
                                        <!-- /.map-toolbar-group -->
                                    </div>
                                    <!-- /.map-toolbar -->
                                    {{--<div class="map-filter-wrapper">--}}
                                        {{--<div class="container-fluid">--}}
                                            {{--<div class="map-filter">--}}
                                                {{--<form>--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<input type="text" class="form-control" placeholder="Keyword">--}}
                                                    {{--</div>--}}
                                                    {{--<!-- /.form-group -->--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<input type="text" class="form-control" placeholder="Location">--}}
                                                    {{--</div>--}}
                                                    {{--<!-- /.form-group -->--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<input type="text" class="form-control" placeholder="Category">--}}
                                                    {{--</div>--}}
                                                    {{--<!-- /.form-group -->--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<input type="text" class="form-control" placeholder="Price">--}}
                                                    {{--</div>--}}
                                                    {{--<!-- /.form-group -->--}}
                                                    {{--<button type="submit" class="btn">Filter Listings</button>--}}
                                                {{--</form>--}}
                                            {{--</div>--}}
                                            {{--<!-- /.map-filter -->--}}
                                        {{--</div>--}}
                                        {{--<!-- /.container -->--}}
                                    {{--</div>--}}
                                    <!-- /.map-filter-wrapper -->
                                </div>
                                <!-- /#map-object -->
                            </div>
                            <!-- /.map-inner -->
                        </div>
                        <!-- /.map -->
                    </div>
                    <!-- /.map-wrapper -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.listing-row-medium').click(function () {


            });

            $(document).on("click", ".listing-row-medium", function () {

                var id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "/api/webpointdetails/",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("point_id").value = id;
                                    $("#point_name").html("" + data.name + "");
                                    $("#point_address").html("" + data.latitude +" - " + data.longitude + "");
                                    $("#point_image").attr("src",""+data.image+"");
                                    $("#point_description").html(""+data.description+"");
                                    break;

                                } else if (data.status === '01') {
                                    alert('Error Fetching Point Details');
                                }
                                break;
                            case "failed":
                                alert('Error Fetching Point Details');
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

        });
    </script>

    @endsection