<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 11:25 PM
 */
?>

@extends('layouts.index')
@section('content')
  <div class="container mt80">
    <div class="row">
      <div class="col-md-8 col-lg-9">
        <!-- /.sort-options -->
        @foreach($points as $point)
        <div class="listing-row">
          <div class="listing-row-inner">
            <a class="listing-row-image" href="{{ url('listed/details/'.$point->id) }}">
              <span class="listing-row-image-content" style="background-image: url({{ asset('uploads/points/web/'.$point->image) }})"></span>
            </a>
            <div class="listing-row-content">
              <div class="listing-row-content-header">
                <h3><a href="{{ url('listed/details/'.$point->id) }}">{{ $point->name }}</a></h3>
                <div class="actions">
                  <a href="#" id="point_id" data-id=""><i class="fa fa-heart fa-2x"></i></a>
                </div>
                <!-- /.actions -->
              </div>
              <!-- /.listing-row-content-header -->
              <div class="listing-row-content-meta">
                <!-- /.listing-row-meta-item -->
                <div class="listing-row-content-meta-item listing-row-content-meta-category">
                  <span class="tag tag-black">{{ strtok($point->category, " ") }}</span>
                </div>
                <!-- /.listing-row-meta-item -->
                <!-- /.listing-row-meta-item -->
              </div>
              <!-- /.listing-row-meta-item -->
              <div class="listing-row-content-body">
                {{ substr($point->description, 0, 190) }}
                <div class="listing-row-content-read-more">
                  <a href="{{ url('listed/details/'.$point->id) }}">Read more</a>
                </div>
                <!-- /.listing-row-content-read-more -->
              </div>
              <!-- /.listing-row-content-body -->
            </div>
            <!-- /.listing-row-content -->
          </div>
          <!-- /.listing-row-inner -->
        </div>
        @endforeach
          {{ $points->links() }}
      </div>
      <!-- /.col -->
      <div class="col-md-4 col-lg-3">
        <div class="sidebar">
          <div class="widget">
            <h3 class="widgettitle">Filter Listings</h3>
            <div class="filter">
              <form method="get" action="?">
                <!-- /.form-group -->
                <!-- /.checkbox -->
                <h2>Categories</h2>
                  @foreach($categories as $category)
                    <div class="checkbox">
                      <label><input type="checkbox" value="{!! $category -> name !!}"> {!! $category -> name !!}
                          <span class="label">{!! $category -> points_count !!}</span>
                      </label>
                    </div>
                  @endforeach
                <!-- /.checkbox -->
                <div class="form-group-btn form-group-btn-placeholder-gap">
                  <button type="submit" class="btn btn-primary btn-block">Search</button>
                </div>
                <!-- /.form-group -->
              </form>
            </div>
            <!-- /.filter -->
          </div>
          <!-- /.widget -->
        </div>
        <!-- /.sidebbar -->
      </div>
      <!-- /.col-* -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
@endsection
