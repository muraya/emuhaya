<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:13 AM
 */?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('partials.css')

    <title>Emuhaya Portal &middot; {!! ucwords(Request::segment(1)) !!}</title>

    @include('partials.js')
</head>
<body class="">
<div class="admin-wrapper">

        @include('admin.partials.sidemenu')

    <!-- /.admin-sidebar -->
    <div class="admin-main">
        @include('admin.partials.header')
        <!-- /.admin-header -->
            @yield('content')
        <!-- /.admin-content -->
         @include('admin.partials.footer')

    </div>
    <!-- /.admin-main -->
</div>
</body>
</html>

