<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/26/2017
 * Time: 3:18 PM
 */
?>

@extends('admin.layouts.master')
@section('content')


    <div class="content">
        <div class="">
            <div class="page-header-title"><h4 class="page-title">Contacts</h4></div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="m-b-30 m-t-0">Contacts Listing</h4>
                                    </div>
                                </div>
                                <table id="contactsTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Email </th>
                                        <th> Subject </th>
                                        <th> Message </th>
                                        <th> Created At </th>
                                        <th> Action </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th> Name </th>
                                        <th> Email </th>
                                        <th> Subject </th>
                                        <th> Message </th>
                                        <th> Created At </th>
                                        <th> Action </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Categories";
            var createModal = $('#createModal');
            var frmCreate = document.getElementById("frmCreate");
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');


            var contactsTable = $('#contactsTable').DataTable({
                pageLength: 10,
                "ajax":  "/contactsdata",
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'subject', name: 'subject'},
                    {data: 'message', name: 'message'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions'}
                ]
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/contacts/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    contactsTable.ajax.reload( null, false );
                                    $('#deleteModal').modal('hide');
                                    toastr.success('Success!', data.message);
                                } else if (data.status === '01') {
                                    toastr.error('Error!', data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

        });
    </script>

@endsection