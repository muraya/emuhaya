<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/2/2017
 * Time: 12:14 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-map-marker"></i>
            </a>
            <h1>Create Point </h1>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="row">
                        <div class="col">
                            @include('partials.flash-message')
                            <form action="{{url('points')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Point Name">
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" id="category" name="category">
                                        <option value=""> -- Select Category -- </option>
                                        @foreach($categories as $category)
                                            <option value="{!! $category -> id !!}">{!! $category -> name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                                </div>

                                <div class="row mb-30">
                                    <div class="col-sm-8">
                                        <div id="location-google-map" style="height: 350px;"></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="location">Location</label>
                                            <input type="text" name="location" id="location" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="image"> Point Image </label>
                                            <input type="file" class="form-control-file" id="image" name="image">
                                            <small id="fileHelp" class="form-text text-muted">
                                                The allowed extensions are only JPG, PNG and JPEG.
                                            </small>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="listing_location_latitude">Latitude</label>
                                            <input type="text" name="latitude" class="form-control" id="listing_location_latitude">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="listing_location_longitude">Longitude</label>
                                            <input type="text" name="longitude" class="form-control" id="listing_location_longitude">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <button type="submit" class="btn btn-primary btn-block m">
                                                <i class="fa fa-upload"></i>
                                                Submit Point
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-inner -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.admin-content -->

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

