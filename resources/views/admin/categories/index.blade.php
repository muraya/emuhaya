<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:18 PM
 */
?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-columns"></i>
            </a>
            <h1> Points Categories</h1>
            <div class="admin-page-title-actions">
                <a href="{{ url('categories/create') }}" class="btn  btn-primary"><i class="fa fa-columns"></i> Create Point Category</a>
            </div>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <!-- /.table-header -->
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="table-wrapper">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Posted By</th>
                                <th>Posts</th>
                                <th class="min-width no-wrap">Status</th>
                                <th class="min-width center">Last Edited</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td class="min-width">
                                        <div class="avatar" style="background-image: url(img/tmp/user-1.jpg)"></div>
                                        <h2>
                                            <a href="#">{!! $category -> name !!}</a>
                                        </h2>
                                    </td>
                                    <td>{!! $category -> description !!}</td>
                                    <td class="min-width">{!! $category -> user_name !!}</td>
                                    <td class="min-width">{!! $category -> posts !!}</td>
                                    <td class="min-width">
                                            @if($category -> status == 1)
                                                <a href="#" title="Deactivate Category" class="btn btn-primary btn-xs deactivate"
                                                   data-id="{!! $category -> id !!}" data-name="{!! $category -> name !!}"
                                                   data-toggle="modal" data-target="#deactivateModal">
                                                    <i class="fa fa-check-circle-o"></i>
                                                </a>
                                            @else
                                                <a href="#" title="Activate Category" class="btn btn-danger btn-xs activate"
                                                   data-id="{!! $category -> id !!}" data-name="{!! $category -> name !!}"
                                                   data-toggle="modal" data-target="#activateModal">
                                                    <i class="fa fa-eraser"></i>
                                                </a>
                                            @endif
                                    </td>
                                    <td class="min-width number">{!! $category -> updated_at !!}</td>
                                    <td class="min-width">
                                        <div class="btn-group">

                                            <a href="/categories/{!! $category->id !!}/edit" title="Edit Category" class="btn btn-primary btn-xs edit"
                                               data-id="{!! $category->id !!}" data-name="{!! $category->name !!}">
                                                <i class="fa fa-edit"></i></a>

                                            <a href="#" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal"
                                               data-id="{!! $category->id !!}" data-name="{!! $category->name !!}" title="Delete Category"
                                               data-target="#deleteModal" > <i class="fa fa-trash-o"></i>
                                            </a>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-wrapper -->
                </div>
                <!-- /.box-inner -->
            </div>
            <!-- /.box -->
            <ul class="pagination pull-right">
                {{ $categories->links() }}
            </ul>
        </div>
        <!-- /.container -->
    </div>

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Category";
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');

            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/categories/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/categories/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/categories/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                pageReload();
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            function pageReload() {
                document.location.reload();
            }


        });
    </script>

@endsection