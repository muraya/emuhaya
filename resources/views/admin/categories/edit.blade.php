<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/2/2017
 * Time: 10:00 AM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-columns"></i>
            </a>
            <h1>Edit {!! $Category -> name !!} Category </h1>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="row">
                        <div class="col">
                            @include('partials.flash-message')
                            <form action="{{'/categories/'.$Category->id}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="form-group">
                                    <label for="name">Category Name</label>
                                    <input type="text" class="form-control" id="name" value="{!! $Category -> name !!}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Category Description</label>
                                    <textarea class="form-control" id="description" name="description" rows="3">{!! $Category -> description !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="image">Category Image</label>
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    <small id="fileHelp" class="form-text text-muted">
                                        The allowed extensions are only JPG, PNG and JPEG.
                                    </small>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <button type="submit" class="btn btn-primary btn-block m">
                                                <i class="fa fa-upload"></i>
                                                Submit Category
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-inner -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.admin-content -->

    {{--MODALS --}}

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

