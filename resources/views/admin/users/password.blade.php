<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/7/2017
 * Time: 6:42 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="content">
        <div class="">
            <div class="page-header-title"><h4 class="page-title">Account Password</h4></div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <h4 class="m-b-30 m-t-0">Change Password Profile</h4>
                                @include('admin.partials.flash-message')
                                <form role="form" action="{{url('admin/password')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="current_password">Current Password</label>
                                            <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password">New Password</label>
                                            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter Model">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirm_password">Repeat Password</label>
                                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Model">
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer col-md-8 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary btn-block m"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

