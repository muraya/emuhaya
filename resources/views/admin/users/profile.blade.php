<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 5/7/2017
 * Time: 6:41 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-map-marker"></i>
            </a>
            <h1> My Profile </h1>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="row">
                        <div class="col">
                            @include('admin.partials.flash-message')
                            <form role="form" action="{{url('admin/profile')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Names</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $User->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" value="{{ $User->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{ $User->phone }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="file">My Image</label>
                                        <p class="help-block"> Make sure the image is of ideal quality and and square in dimensions.</p>
                                        <input type="file" id="file" name="file">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <fieldset>
                                    <div class="box-footer col-md-8 col-md-offset-2">
                                        <button type="submit" class="btn btn-primary btn-block m"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-inner -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.admin-content -->

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

