<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:42 AM
 */
?>
@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-long-arrow-left"></i>
            </a>
            <h1>System Users</h1>
            <div class="admin-page-title-actions">
                {{--<a href="#" class="btn  btn-primary"><i class="fa fa-user-plus"></i> Create User</a>--}}
                <a href="#" class="btn  btn-primary" id="create_user_modal" data-toggle="modal" data-target="#createUserModal">
                    <i class="fa fa-plus"></i> <span>Create User</span>
                </a>
            </div>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <!-- /.table-header -->
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="table-wrapper">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                {{--<th class="min-width no-wrap">Total Listings</th>--}}
                                <th class="min-width center">Active</th>
                                <th class="min-width center">Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                <td>
                                    <div class="avatar" style="background-image: url(img/tmp/user-1.jpg)"></div>
                                    <h2>
                                        <a href="#">{!! $user -> name !!}</a>
                                    </h2>
                                </td>
                                <td class="min-width number">{!! $user -> email !!}</td>
                                <td class="min-width number">{!! $user -> phone !!}</td>
                                {{--<td class="min-width number">1234</td>--}}
                                <td class="min-width center status">
                                        @if($user -> status == 1)
                                            <a href="#" title="Deactivate Point" class="btn btn-primary btn-xs deactivate"
                                               data-id="{!! $user -> id !!}" data-name="{!! $user -> name !!}"
                                               data-toggle="modal" data-target="#deactivateModal">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a>
                                        @else
                                            <a href="#" title="Activate Point" class="btn btn-danger btn-xs activate"
                                               data-id="{!! $user -> id !!}" data-name="{!! $user -> name !!}"
                                               data-toggle="modal" data-target="#activateModal">
                                                <i class="fa fa-thumbs-o-down"></i>
                                            </a>
                                        @endif
                                </td>
                                <td class="min-width center">
                                    @if($user -> role_id == 1)
                                        admin
                                        @else
                                        user
                                    @endif
                                </td>
                                <td class="min-width">
                                    <div class="btn-group">

                                        <a href="#" type="button" class="btn btn-primary btn-xs edit" data-toggle="modal"
                                           data-id="{!! $user -> id !!}"  data-name="{!! $user -> name !!}" title="Delete User"
                                           data-target="#editModal">
                                            <i class="fa fa-edit"></i>
                                        </a>

                                        <a href="#" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal"
                                           data-id="{!! $user -> id !!}" data-name="{!! $user -> name !!}" title="Delete User"
                                           data-target="#deleteModal">
                                            <i class="fa fa-trash-o"></i>
                                        </a>

                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-wrapper -->
                </div>
                <!-- /.box-inner -->
            </div>
            <!-- /.box -->
            <ul class="pagination pull-right">
                {{ $users->links() }}
            </ul>
        </div>
        <!-- /.container -->
    </div>

    {{--MODALS --}}

    <div class="modal inmodal fade" id="createUserModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title"> Create User  </h4>
                </div>
                <div class="modal-body">
                    <form id="frmCreate" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="name">Full Name</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email"> Email</label>
                                <input type="email" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone"> Phone</label>
                                <input type="text" name="phone" id="phone" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="role">User Role</label>
                                <select class="form-control" name="role" id="role">
                                    <option> -- Select Role -- </option>
                                    @foreach($roles as $role)
                                        <option value="{{$role ->  id}}">{{ $role -> name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseCreate" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnCreate" data-dismiss="modal" class="btn btn-primary waves-effect waves-light">Save Details</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="editTitle"> Edit User Details </h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" role="form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="editName">Full Name</label>
                                <input type="text" name="editName" id="editName" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="editEmail"> Email</label>
                                <input type="email" name="editEmail" id="editEmail" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="editPhone"> Phone</label>
                                <input type="text" name="editPhone" id="editPhone" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="editRole">Select Role</label>
                                <select class="form-control" name="editRole" id="editRole">
                                    <option value=""> -- Select Role -- </option>
                                    @foreach($roles as $role)
                                        <option value="{{$role ->  id}}">{{ $role -> name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <input type="hidden" id="entityID">

                        <div class="form-group">
                            <input type="hidden" class="form-control required">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCloseEdit" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" id="btnSaveEdit" data-dismiss="modal" class="btn btn-primary waves-effect waves-light">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "User";
            var createModal = $('#createModal');
            var frmCreate = document.getElementById("frmCreate");
            var editModal = $('#editModal');
            var frmEdit = document.getElementById("frmEdit");
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');

            $("#btnCreate").click(function () {

                var frm = $('#frmCreate');
                var form = document.getElementById("frmCreate");
                var data = frm.serialize();
                $.ajax({
                    type: "POST",
                    url: "/users",
                    data: data,
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#createModal').modal('hide');
                                    frmCreate.reset();
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();                           }
                                break;
                            case "failed":
                                alert(data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on("click", ".edit", function () {

                var btnSaveEdit = $("#btnSaveEdit");
                var id = $(this).data('id');
                var name = $(this).data('name'); // get the item name
                $("#editTitle").html("Edit " + name + ' Details');
                $.ajax({
                    type: "GET",
                    url: "/users/"+id+"/edit",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    document.getElementById("entityID").value = id;
                                    document.getElementById("editName").value = data.name;
                                    document.getElementById("editEmail").value = data.email;
                                    document.getElementById("editPhone").value = data.phone;
                                    document.getElementById("editRole").value = data.role;
                                    break;

                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $("#btnSaveEdit").click(function () {

                var name = $("#editName").val();
                var email = $("#editEmail").val();
                var phone = $("#editPhone").val();
                var role = $("#editRole").val();
                var id = $("#entityID").val();
                $.ajax({
                    type: "PATCH",
                    url: "/users/"+id,
                    data: {
                        id: id,
                        name: name,
                        email: email,
                        role_id: role,
                        phone: phone
                    },
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    editModal.modal('hide');
                                    frmEdit.reset();
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    pageReload();
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + name + ' details');
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/users/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + name + ' Details');
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/users/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                }
                                break;
                            case "failed":
                                toastr.error('Error!', data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + name + ' Details');
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/users/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#deleteModal').modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                }
                                break;
                            case "failed":
                                alert(data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            function pageReload() {
                document.location.reload();
            }


        });
    </script>

@endsection