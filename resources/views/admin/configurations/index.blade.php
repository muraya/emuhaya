<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:21 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-map-marker"></i>
            </a>
            <h1> Configurations </h1>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="row">
                        <div class="col">
                            @include('partials.flash-message')
                            <form role="form" action="{{'/configurations/'.$Configuration->id}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Company Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $Configuration -> name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="main_address">Main Address </label>
                                        <input type="text" class="form-control" id="main_address" name="main_address" value="{{ $Configuration -> main_address }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="other_address"> Other Address</label>
                                        <input type="text" class="form-control" id="other_address" name="other_address" value="{{ $Configuration -> other_address }}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="main_phone">Main Phone</label>
                                        <input type="text" class="form-control" id="main_phone" name="main_phone" value="{{ $Configuration -> main_phone }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="other_phone">Other Phone</label>
                                        <input type="text" class="form-control" id="other_phone" name="other_phone" value="{{ $Configuration -> other_phone }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="admin_email">Administrator Email </label>
                                        <input type="text" class="form-control" id="admin_email" name="admin_email" value="{{ $Configuration -> admin_email }}">
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer col-md-8 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary btn-block m"><i class="fa fa-save"></i> Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-inner -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.admin-content -->

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

