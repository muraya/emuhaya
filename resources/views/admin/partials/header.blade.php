<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:14 AM
 */?>

<div class="admin-header">
    <div class="container-fluid">
        <div class="admin-header-sidebar-toggle toggle">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- /.primary-nav-wrapper -->
        <div class="admin-header-avatar">
            <span class="avatar">
                {{--<img src="" title=""/>--}}
                <img src="/{!!  Auth::user()->avatar !!}" class="user-image" alt="User Image">
            </span>
            <span class="admin-header-avatar-name">{!! Auth::user()->name !!} <i class="fa fa-chevron-down"></i></span>
            <ul class="admin-header-avatar-menu">
                <li><a href="{{ url('admin/profile') }}">Your Profile</a></li>
                <li><a href="{{ url('admin/password') }}">Change Password</a></li>
                <li><a href="#" onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">Sign Out</a>
                    <form id="logout-form"
                          action="{{ url('/logout') }}"
                          method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            <!-- /.admin-header-avatar-menu -->
        </div>
        <!-- /.admin-header-avatar -->
        <!-- /.admin-header-actions -->
    </div>
    <!-- /.container -->
</div>

