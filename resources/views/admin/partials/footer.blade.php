<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:14 AM
 */?>

<div class="admin-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                &copy; 2017 Created by <a href="http://devinvent.com"> Devinvent Technologies</a>. All rights reserved.
            </div>
            <!-- /.col-* -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

