<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/30/2017
 * Time: 2:13 AM
 */?>

<div class="admin-sidebar">
    <div class="admin-sidebar-header">
        <div class="admin-sidebar-logo">
            <a href="{{ url('') }}">
                <img src="/{!!  Auth::user()->avatar !!}" class="svg" alt="Home">
            </a>
            <a href="{{ url('') }}" class="admin-sidebar-logo-title">
                <strong>Emuhaya</strong>
                <span> Administrator</span>
            </a>
        </div>
        <!-- /.admin-sidebar-logo -->
    </div>
    <!-- /.admin-sidebar-header -->

    <div class="admin-sidebar-content">
        <h3> Admin Menu </h3>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a href="{{ url('admin') }}" class="nav-link active">
                    <i class="fa fa-tachometer"></i><span>Dashboard</span>
                </a>
            </li>
            {{--<li class="nav-item">--}}
                {{--<a href="{{ url('admin/users') }}" class="nav-link ">--}}
                    {{--<i class="fa fa-align-justify"></i><span>System Roles</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="nav-item">
                <a href="{{ url('admin/users') }}" class="nav-link ">
                    <i class="fa fa-align-justify"></i><span>System Users</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('admin/categories') }}" class="nav-link ">
                    <i class="fa fa-building"></i><span> Categories</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('admin/points') }}" class="nav-link ">
                    <i class="fa fa-briefcase"></i><span>Points</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('admin/staticmaps') }}" class="nav-link ">
                    <i class="fa fa-briefcase"></i><span>Static Maps</span>
                </a>
            </li>
        </ul>
        <!-- /.nav -->
    </div>
    <!-- /.admin-sidebar-content -->
    <div class="admin-sidebar-footer">
        {{--<strong>Need a help?</strong>--}}
        {{--<span>Call us +0-123-456-789</span>--}}
    </div>
</div>


