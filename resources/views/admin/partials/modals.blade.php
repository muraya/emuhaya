<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 3/8/2017
 * Time: 3:00 PM
 */
?>

    <div class="modal inmodal fade" id="activateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="activateTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="activateNotification"></div>
                    <input type="hidden" id="activateID">
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeActivate" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnActivate" data-dismiss="modal" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal fade" id="deactivateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="deactivateTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="deactivateNotification"></div>
                    <input type="hidden" id="deactivateID">
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeDeactivate" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnDeactivate" data-dismiss="modal" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="deleteTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="deleteNotification"></div>
                    <input type="hidden" id="deleteID">
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeDelete" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" id="btnDelete" data-dismiss="modal" class="btn btn-primary waves-effect waves-light">Delete</button>
                </div>
            </div>
        </div>
    </div>




