<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/3/2017
 * Time: 12:57 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-map"></i>
            </a>
            <h1>Create Static Map </h1>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="row">
                        <div class="col">
                            @include('partials.flash-message')
                            <form action="{{url('staticmaps')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Static Map Name">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="file">Map Image</label>
                                    <input type="file" class="form-control-file" id="file" name="file">
                                        <small id="fileHelp" class="form-text text-muted">
                                        The allowed extensions are only JPG, PNG and JPEG.
                                        </small>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <button type="submit" class="btn btn-primary btn-block m">
                                                <i class="fa fa-upload"></i>
                                                Submit Static Map
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-inner -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.admin-content -->

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>

@endsection

