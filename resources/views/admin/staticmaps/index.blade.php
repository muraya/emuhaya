<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 8/31/2017
 * Time: 4:38 PM
 */

?>

@extends('admin.layouts.master')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-long-arrow-left"></i>
            </a>
            <h1> Static Maps</h1>
            <div class="admin-page-title-actions">
                <a href="{{ url('staticmaps/create') }}" class="btn  btn-primary"><i class="fa fa-map"></i> Post Static Map</a>
            </div>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <!-- /.table-header -->
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="table-wrapper">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Posted BY</th>
                                <th class="min-width no-wrap">Status</th>
                                <th class="min-width center">Last Edited</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($staticmaps as $staticmap)
                                <tr>
                                    <td class="min-width">
                                        {!! $staticmap -> name !!}
                                    </td>
                                    <td class="min-width">{!! $staticmap -> description !!}</td>
                                    <td class="min-width number">{!! $staticmap -> posted_by !!}</td>
                                    <td class="min-width center status">
                                        @if($staticmap -> status == 1)
                                            <a href="#" title="Deactivate Point" class="btn btn-primary btn-xs deactivate"
                                               data-id="{!! $staticmap -> id !!}" data-name="{!! $staticmap -> name !!}"
                                               data-toggle="modal" data-target="#deactivateModal">
                                                <i class="fa fa-thumbs-o-up"></i>
                                            </a>
                                        @else
                                            <a href="#" title="Activate Point" class="btn btn-danger btn-xs activate"
                                               data-id="{!! $staticmap -> id !!}" data-name="{!! $staticmap -> name !!}"
                                               data-toggle="modal" data-target="#activateModal">
                                                <i class="fa fa-thumbs-o-down"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td class="min-width number">{!! $staticmap -> updated_at !!}</td>
                                    <td class="min-width">
                                        <div class="btn-group">

                                            <a href="/staticmaps/{!! $staticmap->id !!}/edit" title="Edit Point" class="btn btn-primary btn-xs edit"
                                               data-id="{!! $staticmap->id !!}" data-name="{!! $staticmap->name !!}">
                                                <i class="fa fa-edit"></i></a>

                                            <a href="#" type="button" class="btn btn-danger btn-xs delete" data-toggle="modal"
                                               data-id="{!! $staticmap->id !!}" data-name="{!! $staticmap->name !!}" title="Delete Point"
                                               data-target="#deleteModal" > <i class="fa fa-trash-o"></i>
                                            </a>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-wrapper -->
                </div>
                <!-- /.box-inner -->
            </div>
            <!-- /.box -->
            <ul class="pagination pull-right">
                {{ $staticmaps->links() }}
            </ul>
        </div>
        <!-- /.container -->
    </div>

    {{--MODALS --}}

    @include('admin.partials.modals')

    <!-- Page-Level Scripts -->
    <script type='text/javascript' charset="utf-8">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var entity = "Static Map";
            var activateModal = $('#activateModal');
            var deactivateModal = $('#deactivateModal');
            var deleteModal = $('#deleteModal');


            $(document).on('click', 'a.activate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("activateID").value = id;
                $("#activateTitle").html("Activate " + entity);
                $("#activateNotification").html("Are you sure you want to activate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnActivate').click(function () {

                var id = $("#activateID").val();
                $.ajax({
                    type: "POST",
                    url: "/staticmaps/activate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    activateModal.modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                alert(data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.deactivate', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deactivateID").value = id;
                $("#deactivateTitle").html("Deactivate " + entity);
                $("#deactivateNotification").html("Are you sure you want to deactivate " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDeactivate').click(function () {

                var id = $("#deactivateID").val();
                $.ajax({
                    type: "POST",
                    url: "/staticmaps/deactivate",
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    deactivateModal.modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                    pageReload();
                                }
                                break;
                            case "failed":
                                alert(data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            $(document).on('click', 'a.delete', function () {

                var id = $(this).data('id'); // get the item ID
                var name = $(this).data('name'); // get the item name
                document.getElementById("deleteID").value = id;
                $("#deleteTitle").html("Delete " + entity);
                $("#deleteNotification").html("Are you sure you want to delete " + entity + " <span class='text-danger'>" + name + "</span>");
            });

            $('#btnDelete').click(function () {

                var id = $("#deleteID").val();
                $.ajax({
                    type: "DELETE",
                    url: "/staticmaps/"+id,
                    data: {id: id},
                    success: function (data, status) {
                        switch (status) {
                            case "success":
                                if (data.status === '00') {
                                    $('#deleteModal').modal('hide');
                                    alert(data.message);
                                    pageReload();
                                } else if (data.status === '01') {
                                    alert(data.message);
                                }
                                break;
                            case "failed":
                                alert(data.message);
                                break;
                            default :
                                alert("do nothing");
                        }
                    }
                });
            });

            function pageReload() {
                document.location.reload();
            }


        });
    </script>

@endsection