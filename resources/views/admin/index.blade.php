<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 4/29/2017
 * Time: 3:36 AM
 */?>
@extends('admin.layouts.dashboard')
@section('content')

    <div class="admin-page-title">
        <div class="container-fluid">
            <a href="{{ url('admin') }}" class="admin-page-title-back">
                <i class="fa fa-dashboard"></i>
            </a>
            <h1>Dashboard </h1>
            <div class="admin-page-title-actions">
                <a href="{{ url('admin/configurations') }}" class="btn btn-secondary"><i class="fa fa-cog"></i> Settings</a>
            </div>
            <!-- /.admin-page-title-actions -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.admin-page-title -->
    <div class="admin-content">
        <div class="container-fluid">
            <div class="stats">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="stat-box stat-box-red">
                            <div class="stat-box-icon"><i class="fa fa-pie-chart"></i></div>
                            <strong>{!! count($categories) !!}</strong>
                            <span>Point Categories</span>
                        </div>
                        <!-- /.stat-box -->
                    </div>
                    <!-- /.col-* -->
                    <div class="col-md-6 col-lg-3">
                        <div class="stat-box stat-box-green">
                            <div class="stat-box-icon"><i class="fa fa-comments"></i></div>
                            <strong>{!! count($points) !!}</strong>
                            <span>Points Posted</span>
                        </div>
                        <!-- /.stat-box -->
                    </div>
                    <!-- /.col-* -->
                    <div class="col-md-6 col-lg-3">
                        <div class="stat-box stat-box-blue">
                            <div class="stat-box-icon"><i class="fa fa-diamond"></i></div>
                            <strong>{!! count($staticmaps) !!}</strong>
                            <span>Static Maps</span>
                        </div>
                        <!-- /.stat-box -->
                    </div>
                    <!-- /.col-* -->
                    <div class="col-md-6 col-lg-3">
                        <div class="stat-box stat-box-purple">
                            <div class="stat-box-icon"><i class="fa fa-database"></i></div>
                            <strong>{!! count($users) !!}</strong>
                            <span>System Users</span>
                        </div>
                        <!-- /.stat-box -->
                    </div>
                    <!-- /.col-* -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.stats -->
            <div class="box">
                <div class="box-inner">
                    <!-- /.box-title -->
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Active Point Categories
                                    <span>
                                        {!! intval((count($activeCategories)/ count($categories)) * 100 ) !!} %
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($activeCategories)/ count($categories)) * 100) !!}%">
                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Inactive Point Categories
                                    <span>
                                        {!! intval((count($inactiveCategories)/ count($categories)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($inactiveCategories)/ count($categories)) * 100) !!}%">
                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Active Points
                                    <span>
                                        {!! intval((count($activePoints)/ count($points)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($activePoints)/ count($points)) * 100) !!}%">

                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Inactive Points
                                    <span>
                                        {!! intval((count($inactivePoints)/ count($points)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($inactivePoints)/ count($points)) * 100) !!}%">

                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Active Static Maps
                                    <span>
                                        {!! intval((count($activeStaticMaps)/ count($staticmaps)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($activeStaticMaps)/ count($staticmaps)) * 100) !!}%">

                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                   Inactive Static Maps
                                    <span>
                                        {!! intval((count($inactiveStaticMaps)/ count($staticmaps)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($inactiveStaticMaps)/ count($staticmaps)) * 100) !!}%">

                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Active System users
                                    <span>
                                        {!! intval((count($activeUsers)/ count($users)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($activeUsers)/ count($users)) * 100 ) !!}%">

                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                            <div class="stat-progress">
                                <div class="stat-progress-label">
                                    Inactive System Users
                                    <span>
                                        {!! intval((count($inactiveUsers)/ count($users)) * 100) !!}%
                                    </span>
                                </div>
                                <div class="stat-progress-bar">
                                    <div class="stat-progress-bar-inner"
                                         style="width: {!! intval((count($inactiveUsers)/ count($users)) * 100) !!}%">

                                    </div>
                                    <!-- /.stat-progress-inner -->
                                </div>
                                <!-- /.stat-progress -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-inner -->
            </div>
        </div>
        <!-- /.container -->
    </div>

@endsection
