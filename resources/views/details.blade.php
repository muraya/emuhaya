<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 9/1/2017
 * Time: 1:09 AM
 */
?>

@extends('layouts.index')
@section('content')
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="listing-hero">
                    <div class="listing-hero-inner">
                        <div class="container">
                            <div class="listing-hero-image" style="background-image: url({{ asset('uploads/points/mobile/'.$Point->image) }})"></div>
                            <!-- /.listing-hero-image -->
                            <h1>{!! $Point -> name !!} <i class="fa fa-check"></i></h1>
                            <address>
                                {!! $Point -> lat !!}<br>
                                {!! $Point -> lng !!}
                            </address>
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.listing-hero-inner -->
                </div>
                <!-- /.listing-hero -->
                <div class="listing-toolbar-wrapper">
                    <div class="listing-toolbar" data-spy="affix" data-offset-top="203">
                        <div class="container">
                            <ul class="nav">
                            </ul>
                            <!-- /.nav -->
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.listing-toolbar -->
                </div>
                <!-- /.listing-toolbar-wrapper -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-lg-9">
                            <div class="listing-detail-section" id="listing-detail-section-description" data-title="Description">
                                <div class="gallery">
                                    <div class="gallery-item" style="background-image: url({{ asset('uploads/points/web/'.$Point->image) }});">
                                    </div>
                                    <!-- /.gallery-item -->
                                </div>
                                <!-- /.gallery -->
                                <div class="row">
                                    <!-- /.col-* -->
                                    <div class="col-lg-12">
                                        <h2>Description</h2>
                                        <p>
                                            {!! $Point->description !!}
                                        </p>
                                    </div>
                                    <!-- /.col-* -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.listing-detail-section -->
                            <div class="listing-detail-section" id="listing-detail-section-map-position" data-title="Map Position">
                                <h2>Map Position</h2>
                                <iframe class="mb30" style="height:320px;width:100%;border:0;"
                                        src="https://www.google.com/maps/embed/v1/place?q={!! $Point->lat !!},{!! $Point->lng !!}&key=AIzaSyCxhpj8uRh5jWdDSi8dt3RnOKWaEGFHIZM">
                                </iframe>
                            </div>
                            <!-- /.listing-detail-section -->
                        </div>
                        <!-- /.col-* -->
                        <div class="col-md-4 col-lg-3">
                            <div class="sidebar">
                                {{--<div class="widget">--}}
                                    {{--<ul class="nav actions flex-column">--}}
                                        {{--<li class="nav-item featured">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-file-text-o"></i> Claim Listing--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-star-o"></i> Add to Favorites--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-flag"></i> Report Listing--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link" onclick="window.print(); return false;">--}}
                                                {{--<i class="fa fa-print"></i> Print page--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-facebook"></i> Share on Facebook--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-google"></i> Share on Google+--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-twitter"></i> Share on Twitter--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a href="#" class="nav-link">--}}
                                                {{--<i class="fa fa-linkedin"></i> Share on LinkedIn--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                <div class="widget">
                                    <div class="author-small">
                                        <div class="avatar" style="background-image: url({{ asset('uploads/profile/'.$User->avatar) }});"></div>
                                        <h3>{!! $User -> name !!}</h3>
                                        <p>Authored {!! count($listings) !!} unique listings.</p>
                                        <div class="author-small-actions">
                                            <p>{!! $User -> phone !!}</p>
                                            <p>{!! $User -> email !!}</p>
                                        </div>
                                        <!-- /.author-small-actions -->
                                    </div>
                                    <!-- /.author-small -->
                                </div>
                                <!-- /.widget -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                        <!-- /.col-* -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.container-fluid -->
@endsection