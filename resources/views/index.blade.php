
@extends('layouts.index')
@section('content')
    <div class="map-wrapper">
        <div class="map" style="height: 680px;">
            <div class="map-inner">
                <div class="map-object">
                    <div id="map-object"></div>
                    <div class="map-toolbar">
                        <div class="map-toolbar-group">
                            <div id="map-toolbar-action-zoom-in" class="map-toolbar-group-item"><i class="fa fa-plus"></i></div>
                            <!-- /.map-toolbar-group-item -->
                            <div id="map-toolbar-action-zoom-out" class="map-toolbar-group-item"><i class="fa fa-minus"></i></div>
                            <!-- /.map-toolbar-group-item -->
                        </div>
                        <!-- /.map-toolbar-group -->
                        <div class="map-toolbar-group">
                            <div id="map-toolbar-action-current-position" class="map-toolbar-group-item"><i class="fa fa-location-arrow"></i></div>
                            <!-- /.map-toolbar-group-item -->
                            <div id="map-toolbar-action-fullscreen" class="map-toolbar-group-item"><i class="fa fa-arrows-alt"></i></div>
                            <!-- /.map-toolbar-group-item -->
                        </div>
                        <!-- /.map-toolbar-group -->
                        <div class="map-toolbar-group">
                            <div id="map-toolbar-action-roadmap" class="map-toolbar-group-item">Roadmap</div>
                            <!-- /.map-toolbar-group-item -->
                            <div id="map-toolbar-action-satellite" class="map-toolbar-group-item">Satellite</div>
                            <!-- /.map-toolbar-group-item -->
                            <div id="map-toolbar-action-terrain" class="map-toolbar-group-item">Terrain</div>
                            <!-- /.map-toolbar-group-item -->
                        </div>
                        <!-- /.map-toolbar-group -->
                    </div>
                    <!-- /.map-toolbar -->
                    <div class="map-filter-wrapper">
                        <div class="container-fluid">
                            <div class="map-filter">
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Keyword">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Location">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Category">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Tag">
                                    </div>
                                    <!-- /.form-group -->
                                    <button type="submit" class="btn">Filter Listings</button>
                                </form>
                            </div>
                            <!-- /.map-filter -->
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.map-filter-wrapper -->
                </div>
                <!-- /#map-object -->
            </div>
            <!-- /.map-inner -->
        </div>
        <!-- /.map -->
    </div>
    <!-- /.map-wrapper -->
    <div class="cta-small">
        <div class="container">
            Welcome to <strong> Emuhaya Cultural Web Portal</strong> where you can view and submit distinctive points within our region. See the  <a href="#">feaures listings</a>.
        </div>
        <!-- /.container -->
    </div>
    <!-- /.cta-small -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="content">
                        <div class="mt-80">
                            <div class="listing-tiles">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="listing-tile">
                                            <div class="listing-tile-image" style="background-image: url({{ asset('uploads/featured/featured-1.jpg') }});">
                                            </div>
                                            <!-- /.listing-tile-image -->
                                            <div class="listing-tile-content">
                                                <span class="tag">Culture</span>
                                                <h2>Pot Making</h2>
                                            </div>
                                            <!-- /.listing-tile-content -->
                                        </div>
                                        <!-- /.listing-tile -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-8">
                                        <div class="listing-tile">
                                            <div class="listing-tile-image" style="background-image: url({{ asset('uploads/featured/featured-2.jpg') }});">
                                            </div>
                                            <!-- /.listing-tile-image -->
                                            <div class="listing-tile-content">
                                                <span class="tag">Agriculture</span>
                                                <h2>Tea Plantations in Emuhaya</h2>
                                            </div>
                                            <!-- /.listing-tile-content -->
                                        </div>
                                        <!-- /.listing-tile -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="listing-tile">
                                            <div class="listing-tile-image" style="background-image: url({{ asset('uploads/featured/featured-3.jpg') }});">
                                            </div>
                                            <!-- /.listing-tile-image -->
                                            <div class="listing-tile-content">
                                                <span class="tag">Technology</span>
                                                <h2>Emuhaya Technologies</h2>
                                            </div>
                                            <!-- /.listing-tile-content -->
                                        </div>
                                        <!-- /.listing-tile -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-6">
                                        <div class="listing-tile">
                                            <div class="listing-tile-image" style="background-image: url({{ asset('uploads/featured/featured-4.jpg') }});">
                                            </div>
                                            <!-- /.listing-tile-image -->
                                            <div class="listing-tile-content">
                                                <span class="tag">Education</span>
                                                <h2>Emuhaya Secondary School</h2>
                                            </div>
                                            <!-- /.listing-tile-content -->
                                        </div>
                                        <!-- /.listing-tile -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3">
                                        <div class="listing-tile">
                                            <div class="listing-tile-image" style="background-image: url({{ asset('uploads/featured/featured-5.jpg') }});">
                                            </div>
                                            <!-- /.listing-tile-image -->
                                            <div class="listing-tile-content">
                                                <span class="tag">Leadership</span>
                                                <h2>Emuhaya County Goverment</h2>
                                            </div>
                                            <!-- /.listing-tile-content -->
                                        </div>
                                        <!-- /.listing-tile -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.listing-tiles -->
                        </div>
                        <div class="block mb0">
                            <div class="block-inner">
                                <div class="page-header">
                                    <h2>Recent Listing</h2>
                                    <p>See the list of recent uploaded listings</p>
                                </div>
                            </div>
                            <!-- /.block-inner -->
                        </div>
                        <!-- /.block-inner -->
                        <div class="carousel-fullwidth">
                            <div class="carousel-fullwidth-inner">
                                <div class="listing-boxes">
                                    <div class="row mb-30 carousel-items">
                                        @foreach($points as $point)
                                        <div class="col">
                                            <div class="listing-box">
                                                <div class="listing-box-inner">
                                                    <a href="{{ url('listed/details/'.$point->id) }}" class="listing-box-image">
                                                        <span class="listing-box-image-content" style="background-image: url({{ asset('uploads/points/web/'.$point->image) }})"></span><!-- /.listing-box-image-content -->
                                                        <span class="listing-box-category tag">{{ strtok($point->category, " ") }}</span>
                                                    </a><!-- /.listing-box-image -->
                                                    <div class="listing-box-content">
                                                        <h2><a href="{{ url('listed/details/'.$point->id) }}">{{ $point->name }}</a></h2>
                                                        <h3><i class="fa fa-map-marker"></i>  {!! $point -> lat !!} - {!! $point -> lng !!}</h3>
                                                        <div class="actions">
                                                            <div class="actions-button">
                                                                <i class="fa fa-heart"></i>
                                                            </div>
                                                            <!-- /.actions-button -->
                                                        </div>
                                                        <!-- /.actions -->
                                                    </div>
                                                </div>
                                                <!-- /.listing-box-inner -->
                                            </div>
                                            <!-- /.listing-box -->
                                        </div>
                                            @endforeach
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.listing-boxes -->
                            </div>
                            <!-- /.carousel-fullwidth-inner -->
                        </div>
                        <!-- /.carousel-fullwidth -->
                        <div class="cta-price">
                            <h3>Get Everything<br>
                                Exlusively in Emuhaya</h3>
                            <h4>Emuhaya Portal</h4>
                        </div>
                        <!-- /.cta-price -->
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->
    @endsection
