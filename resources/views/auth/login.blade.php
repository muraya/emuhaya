@extends('layouts.app')

@section('content')
<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="page-title">
                <div class="container">
                    <h1>Login</h1>
                    <!-- /.page-title-actions -->
                </div>
                <!-- /.container-->
            </div>
            <!-- /.page-title -->
            <div class="container">
                <div class="row mb80 mt80">
                    <div class="col-sm-4 offset-sm-4">
                        <h3 class="page-title-small">Login</h3>
                            <form method="post" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="login-form-email">E-mail</label>
                                    <input type="email" class="form-control" name="email" id="login-form-email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="login-form-password">Password</label>
                                    <input type="password" class="form-control" name="password" id="login-form-password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- /.form-group -->
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                                <button type="submit" class="btn btn-primary pull-right">Login</button>
                            </form>

                    </div>
                    <div class="col-sm-4 offset-sm-4 mt-5 text-center">
                        <a class="text-success" href="{{ route('register') }}">
                            Create Account
                        </a>
                    </div>
                    <!-- /.col-sm-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.main-inner -->
    </div>
    <!-- /.main -->
</div>

@endsection
