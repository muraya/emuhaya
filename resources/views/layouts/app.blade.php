

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('partials.css')

    @include('partials.js')

    <title>Emuhaya Portal &middot; {!! ucwords(Request::segment(1)) !!}</title>
</head>
<body class="">
<div class="page-wrapper">
    <div class="header-wrapper">
        <div class="header">
            <div class="container">
                <div class="header-inner">
                    <div class="navigation-toggle toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <!-- /.header-toggle -->
                    <div class="header-logo">
                        <a href="{{ url('') }}">
                            <img src="{{ asset('img/logo.svg') }}" class="svg" alt="Home">
                        </a>
                        <a href="{{ url('') }}" class="header-title">Emuhaya</a>
                    </div>
                    <!-- /.header-logo -->
                </div>
                <!-- /.header-inner -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.header -->
    </div>
    <!-- /.header-wrapper -->
    @yield('content')
    <!-- /.main-wrapper -->
</div>
<!-- /.page-wrapper -->
<div class="side-overlay"></div>
<!-- /.side-overlay -->

</body>
</html>
