<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 9:30 PM
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    @include('partials.css')

    @include('partials.js')

    <title>Emuhaya Portal &middot; {!! ucwords(Request::segment(1)) !!}</title>

  </head>
  <body class="">
    <div class="page-wrapper">

        @include('partials.header')
        @include('admin.partials.flash-message')
      <!-- /.header-wrapper -->

            @yield('content')

        @include('partials.footer')
      <!-- /.footer-wrapper -->
    </div>
    <!-- /.page-wrapper -->

    @include('partials.submitpoint')

    @include('partials.login')

    <!-- /.side-wrapper -->
    <div class="side-overlay"></div>
    <!-- /.side-overlay -->

  </body>

</html>