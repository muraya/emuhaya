<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 11:27 PM
 */
?>

@extends('layouts.index')
@section('content')
    <!-- /.header-wrapper -->
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="page-title">
                    <div class="container-fluid">
                        <h1>Web Maps</h1>
                        <div class="page-title-actions">
                            <div class="switcher">
                                <strong>Currency</strong>
                                <ul>
                                    <li class="active"><a href="#">USD</a></li>
                                    <li><a href="#">EUR</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                            <div class="switcher">
                                <strong>Language</strong>
                                <ul>
                                    <li class="active"><a href="#">EN</a></li>
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">DE</a></li>
                                </ul>
                            </div>
                            <!-- /.switcher -->
                        </div>
                        <!-- /.page-title-actions -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.page-title -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-md-3 sidebar-wrapper-col">
                            <div class="sidebar">
                                <div class="filter">
                                    <h2>Search criteria</h2>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input type="text" class="form-control" placeholder="Search by keyword ...">
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>City</label>
                                        <select class="form-control">
                                            <option>Location</option>
                                            <option>Chicago</option>
                                            <option>San Francisco</option>
                                            <option>Seattle</option>
                                            <option>New York</option>
                                            <option>Washington</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Price Slider</label>
                                        <div id="slider" class="price-slider"></div>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Amenities</label>
                                        <select class="form-control">
                                            <option>Air conditioning</option>
                                            <option>Balcony</option>
                                            <option>Bedding</option>
                                            <option>Cable TV</option>
                                            <option>Cleaning after exit</option>
                                            <option>Cofee pot</option>
                                            <option>Computer</option>
                                            <option>Cot</option>
                                            <option>Dishwasher</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="row">
                                        <div class="form-group col-xl-6">
                                            <label>Price From</label>
                                            <input type="text" class="form-control" placeholder="Min. price">
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group col-xl-6">
                                            <label>Price To</label>
                                            <input type="text" class="form-control" placeholder="Max. price">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.row -->
                                    <h2>Category</h2>
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="rent"> Property <span class="label">78</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="sale"> Restaurant <span class="label">54</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="sale"> Travel <span class="label">50</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="lease"> Food &amp; Drink <span class="label">32</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="radio" name="form-property-type" value="lease"> Events <span class="label">53</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <h2>Attributes</h2>
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Reduced <span class="label">93</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Featured <span class="label">54</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="checkbox">
                                        <label><input type="checkbox"> Verified <span class="label">22</span></label>
                                    </div>
                                    <!-- /.checkbox -->
                                    <div class="form-group-btn form-group-btn-placeholder-gap">
                                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.filter -->
                            </div>
                            <!-- /.sidebar -->
                        </div>
                        <!-- /.col-* -->
                        <div class="col-sm-8 col-md-9">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-4.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Courses</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Custom Lawn Care</a></h2>
                                                    <h3>2287 Maple Avenue Stockton</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-22.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Law</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Freelance Office</a></h2>
                                                    <h3>4458 Thompson Drive Richmond</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-24.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Business</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Little Tavern</a></h2>
                                                    <h3>2164 Hilltop Haven Drive Newark</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <div class="listing-box-info">
                                                    Currently viewing <strong>8</strong> people.
                                                </div>
                                                <!-- /.listing-box-info -->
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-2.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Gastronomy</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Piano Lessons For Beginners</a></h2>
                                                    <h3>3071 Ash Avenue Saint Louis</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-15.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Driving</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Royal Hall</a></h2>
                                                    <h3>3690 Tecumsah Lane Cedar Rapids</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-3.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Museum</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Erb Lumber</a></h2>
                                                    <h3>2155 Philli Lane Mcalester</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <div class="listing-box-info">
                                                    Special opening prices available.
                                                </div>
                                                <!-- /.listing-box-info -->
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-14.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Mechanics</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Skateshop</a></h2>
                                                    <h3>3298 Lewis Street Downers Grove</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <div class="listing-box-info">
                                                    <strong>120</strong> people already purchased.
                                                </div>
                                                <!-- /.listing-box-info -->
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-16.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Health</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Tower Excursion</a></h2>
                                                    <h3>1081 Woodside Circle Crestview</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <div class="listing-box-info">
                                                    Most visited monthly trendsetter.
                                                </div>
                                                <!-- /.listing-box-info -->
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-2.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Events</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Griff's Hamburgers</a></h2>
                                                    <h3>3228 Hillview Street Columbia</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <div class="listing-box-info">
                                                    Last two spots open.
                                                </div>
                                                <!-- /.listing-box-info -->
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-9.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Greenpeace</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Photo Services</a></h2>
                                                    <h3>3074 Asylum Avenue Danbury</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-21.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Food</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Sushi Bar</a></h2>
                                                    <h3>2464 Par Drive Los Angeles</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                    <div class="col-md-6 col-lg-3">
                                        <div class="listing-box">
                                            <div class="listing-box-inner">
                                                <a href="listings-detail.html" class="listing-box-image">
                                                    <span class="listing-box-image-content" style="background-image: url(img/tmp/listing-10.jpg)"></span><!-- /.listing-box-image-content -->
                                                    <span class="listing-box-category tag">Music</span>
                                                    <span class="listing-box-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                              </span>
                                                </a><!-- /.listing-box-image -->
                                                <div class="listing-box-content">
                                                    <h2><a href="listings-detail.html">Nightlife Party Club</a></h2>
                                                    <h3>1542 Rocket Drive Minneapolis</h3>
                                                    <div class="actions">
                                                        <div class="actions-button">
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                        <!-- /.actions-button -->
                                                        <ul class="actions-list">
                                                            <li><a href="#">Add to compare</a></li>
                                                            <li><a href="#">Add to favorites</a></li>
                                                            <li><a href="#">Report listing</a></li>
                                                        </ul>
                                                        <!-- /.actions-list -->
                                                    </div>
                                                    <!-- /.actions -->
                                                </div>
                                                <!-- /.listing-box-content -->
                                                <div class="listing-box-attributes">
                                                    <dl>
                                                        <dt>Miles</dt>
                                                        <dd>128K</dd>
                                                        <dt>Engine</dt>
                                                        <dd>Diesel</dd>
                                                        <dt>Type</dt>
                                                        <dd>Aston Martin</dd>
                                                    </dl>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                                <div class="listing-box-attributes-icons">
                                                    <ul>
                                                        <li><i class="fa fa-arrows"></i> <span>182sqft</span></li>
                                                        <li><i class="fa fa-shower"></i> <span>2</span></li>
                                                        <li><i class="fa fa-car"></i> <span>1</span></li>
                                                    </ul>
                                                </div>
                                                <!-- /.listing-box-attributes -->
                                            </div>
                                            <!-- /.listing-box-inner -->
                                        </div>
                                        <!-- /.listing-box -->
                                    </div>
                                    <!-- /.col-* -->
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="pagination pull-right">
                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </div>
                                    <!-- /.col-* -->
                                </div>
                            </div>
                            <!-- /.content -->
                        </div>
                        <!-- /.col-* -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>
    <!-- /.main-wrapper -->
@endsection
