<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 11:11 PM
 */
?>

@extends('layouts.error')
@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="container">

                    <div class="warning">
                        <h1>500</h1>
                        <p>
                            We have had an issue processing your request.Please Try again and if it persists report to the administrator.
                        </p>
                        <a href="{{ url('') }}" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i> Return Home</a>
                    </div>

                    <!-- /.warning -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>

@endsection
