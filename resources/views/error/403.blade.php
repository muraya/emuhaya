<?php
/**
 * Created by PhpStorm.
 * User: Muraya
 * Date: 7/11/2017
 * Time: 11:09 PM
 */
?>

@extends('layouts.error')
@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="warning">
                        <h1>403</h1>
                        <p>You are not allowed to access this page.</p>
                        <a href="{{ url('/') }}" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i> Return Home</a>
                    </div>
                    <!-- /.warning -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.main-inner -->
        </div>
        <!-- /.main -->
    </div>

@endsection
